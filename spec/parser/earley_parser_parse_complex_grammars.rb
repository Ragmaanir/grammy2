
describe EarleyParser,"#parse complex grammars" do

	PRIVATE_METHODS = [
		:current_set,
		:predict,
		:predict_items_for,
		:scan,
		:scan_for_item,
		:item_sets,
		:completed_items_for,
		:complete
	]
	
	EarleyParser.send(:public,*PRIVATE_METHODS)
	
	it "nested parenthesized literals" do
		g = Grammar.define do
			start exp => ('(' >> exps? >> ')') | lit
			rule exps => exp >> ~(',' >> exp)
			rule lit => int | str
			token int => '0' | ((1..9) >> ~(0..9))
			token str => +(('a'..'z') | ('A'..'Z'))
		end
		
		parser = EarleyParser.new(g)
		
		parser.parse("(1534)")
		parser.parse("(a)")
		parser.parse("(abSdf)")
		parser.parse("(as,BG)")
		parser.parse("(1,1)")
		parser.parse("(12,(abZ),55,(1,2,a))")
		parser.parse("(12,(1,(5,5),a),(((abc,123),5)))")
		parser.parse("(12,(1,(5,5),a),(((abc,123),5)),(aaaaaaaafffff,((((a,b,56756756757)),786769678678)),44234,(a,b,(1,(5,5),a),(((abc,123),5)))))")
		#p parser.item_sets.map(&:length).inject(&:+)
	end
	
	it "parenthesized literals" do
		g = Grammar.define do
			start s => parlist?
			rule parlist => '(' >> args? >> ')'
			rule args => lit >> ~(',' >> lit)
			rule lit => int | str
			token int => '0' | ((1..9) >> ~(0..9))
			token str => +(('a'..'z') | ('A'..'Z'))
		end
		
		parser = EarleyParser.new(g)
		
		parser.parse("(1)")
		parser.parse("(1,1)")
		parser.parse("(12,abZ,55)")
	end
	
	it "list of optionals" do
		g = Grammar.define do
			start s => optlist
			rule optlist => a? >> b? >> a?
			token a => 'a'
			token b => 'b'
		end
		
		parser = EarleyParser.new(g)
		
		parser.parse("aba")
		parser.parse("a")
		parser.parse("ba")
	end
	
	it "programming language" do
		g = Grammar.define do
			token id => /[a-zA-Z_]+/
			token int => /[0-9]+/
			
			rule stat => id >> '=' >> exp >> "\n"
			rule exp => mul >> ~(('+' | '-') >> mul)
			rule mul => primary >> ~(('*'|'/') >> primary)
			rule primary => id | int
			
			start program => +stat
		end
		
		parser = EarleyParser.new(g)
		
		alpha = ('a'..'z').to_a
		num = ('0'..'9').to_a
		
		randstr = ->(alphabet,n){ n.times.map{alphabet.sample}.join }
		randname = ->(n){ randstr[alpha,n] }
		randnum = ->(n){ randstr[num,n] }
		randprim = ->(n){ rand(1)==0 ? randname[n] : randnum[n] }
		
		str = 800.times.map {
			randname[10] << '=' <<
			(rand(5)+1).times.map{randprim[10]}.join('+')
		}.map{|s| s << "\n"}.join
		
		parser.parse(str)
	end
	
end
