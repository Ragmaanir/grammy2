
describe EarleyParser do
	
	before do
		@simple_gr = Grammar.define do
			token b => 'b'
			rule bs => (b >> bs) | b
			start x => 'a' >> bs
		end
		
		@rule_x = @simple_gr.rules[:x]
		@rule_bs = @simple_gr.rules[:bs]
		@rule_b = @simple_gr.rules[:b]
	end
	
	#
	# EarleyParser::Dot
	#
	describe EarleyParser::Dot do
		it "should be equal" do
			dot1 = EarleyParser::Dot.before(@rule_x)
			dot2 = EarleyParser::Dot.before(@rule_x)
			dot1.should ==(dot2)
		end
		
		it "should not be equal when positions different" do
			dot1 = EarleyParser::Dot.before(@rule_x)
			dot2 = EarleyParser::Dot.after(@rule_x)
			dot1.should_not ==(dot2)
		end
		
		it "should not be equal when rules different" do
			dot1 = EarleyParser::Dot.before(@rule_x)
			dot2 = EarleyParser::Dot.before(@rule_x.children.first)
			dot1.should_not ==(dot2)
		end
		
		it "should raise when invalid parameters" do
			expect{ EarleyParser::Dot.before(nil) }.to raise_error
			expect{ EarleyParser::Dot.new(nil,@rule_x) }.to raise_error
		end
	end
	
	#
	# EarleyParser::Item spec
	#
	describe EarleyParser::Item do
		before do
			@default_item = EarleyParser::Item.new(
				:start => 0,
				:end => 0,
				:rule => @rule_x,
				:dot => EarleyParser::Dot.before(@rule_x)
			)
		end
		
		it "should create from another item" do
			another = @default_item
			
			EarleyParser::Item.create_from(another,:end => 7).should == EarleyParser::Item.new(
				:start => 0,
				:end => 7,
				:rule => @rule_x,
				:dot => EarleyParser::Dot.before(@rule_x)
			)
		end
		
		it "should have empty completion set" do
			@default_item.completing.should be_empty
		end
		
		it "should be equal" do
			default = EarleyParser::Item.new(
				:start => 0,
				:end => 0,
				:rule => @rule_x,
				:dot => EarleyParser::Dot.before(@rule_x)
			)
			
			item1 = EarleyParser::Item.create_from(default,
				:dot => EarleyParser::Dot.before(@rule_x)
			)
			
			item2 = EarleyParser::Item.create_from(default,
				:dot => EarleyParser::Dot.before(@rule_x)
			)
			
			item1.should ==(item2)
		end
		
		it "should not be equal when dot different" do
			default = EarleyParser::Item.new(
				:start => 0,
				:end => 0,
				:rule => @rule_x,
				:dot => EarleyParser::Dot.before(@rule_x)
			)
			
			item1 = EarleyParser::Item.create_from(default,
				:dot => EarleyParser::Dot.before(@rule_x)
			)
			
			item2 = EarleyParser::Item.create_from(default,
				:dot => EarleyParser::Dot.before(@rule_x.children.last)
			)
			
			item1.should_not ==(item2)
		end
		
		it "should raise when rule referenced in dot is not part of rule referenced in item" do
			expect{ 
				EarleyParser::Item.new(
					:start => 0,
					:end => 0,
					:rule => @rule_x,
					:dot => EarleyParser::Dot.before(@rule_bs)
				)
			}.to raise_error
		end
		
		it "should be complete when dot after rule" do
			item = EarleyParser::Item.new(
				:start => 0,
				:end => 0,
				:rule => @rule_x,
				:dot => EarleyParser::Dot.after(@rule_x)
			)
			
			item.should be_complete
		end
		
		it "should be active when dot before subrule" do
			item = EarleyParser::Item.new(
				:start => 0,
				:end => 0,
				:rule => @rule_bs,
				:dot => EarleyParser::Dot.before(@rule_bs.children.first.children[1])
			)
			
			item.should be_active
		end
		
		it "should be active when dot after subrule" do
			item = EarleyParser::Item.new(
				:start => 0,
				:end => 0,
				:rule => @rule_bs,
				:dot => EarleyParser::Dot.after(@rule_bs.children.first.children[1])
			)
			
			item.should be_active
		end
		
		it "should be active when dot before rule" do
			item = EarleyParser::Item.new(
				:start => 0,
				:end => 0,
				:rule => @rule_x,
				:dot => EarleyParser::Dot.before(@rule_x)
			)
			
			item.should be_active
		end
	end
end
