
require 'benchmark'
require 'active_support/time'
require 'perftools'
require 'ruby-prof'

RSpec::Matchers.define :take_less_than do |n|
  #chain :seconds do; end
  #chain :second do; end

  match do |block|
    @elapsed = Benchmark.realtime do
      block.call
    end
    @elapsed <= n
  end

end


describe EarleyParser,"benchmark" do

	PRIVATE_METHODS = [
		:current_set,
		:predict,
		:predict_items_for,
		:scan,
		:scan_for_item,
		:item_sets,
		:completed_items_for,
		:complete
	]
	
	EarleyParser.send(:public,*PRIVATE_METHODS)
	
	it "nested parenthesized literals" do
		pending
		g = Grammar.define do
			start exp => ('(' >> exps? >> ')') | lit
			rule exps => exp >> ~(',' >> exp)
			rule lit => int | str
			token int => '0' | ((1..9) >> ~(0..9))
			token str => +(('a'..'z') | ('A'..'Z'))
		end
		
		parser = EarleyParser.new(g)
		
		#expect {
		#	
		#}.to take_less_than(1.seconds)
		
		#PerfTools::CpuProfiler.start("temp/add_numbers_profile") do
		#	20.times{ parser.parse("(1,2,(a,Z,2),(1))") }
		#end
		
		result = RubyProf.profile do
			20.times{ parser.parse("(1,2,(a,Z,2),(1))") }
		end
		
		printer = RubyProf::GraphHtmlPrinter.new(result)
		#printer.print(File.new("temp/prof.html"))
		File.open("temp/rubyprof_#{Time.now.to_i}.html", 'w') do |file|
			printer.print(file)
		end
	end
	
end
