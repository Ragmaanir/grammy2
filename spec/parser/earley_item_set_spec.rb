
describe EarleyParser::ItemSet do
	
	before do
		@grammar = Grammar.define do
			rule s => a >> b
			rule a => 'a'
			rule b => 'b'
		end
		
		@rule_s = @grammar.rules[:s]
		@rule_a = @grammar.rules[:a]
		@rule_b = @grammar.rules[:b]
		
		@set = EarleyParser::ItemSet.new
	end
	
	describe "should equal" do
		it "regular empty set" do
			EarleyParser::ItemSet.new.should == [].to_set
		end
		it "regular set containing items" do
			item = EarleyParser::Item.new(
				:start => 0, :end => 0,
				:rule => @rule_s,
				:dot => EarleyParser::Dot.before(@rule_s)
			)
			
			is = EarleyParser::ItemSet.new
			is.add(item)
				
			is.should == [item].to_set
		end
	end

	it "should add item to empty set" do
		new_item = EarleyParser::Item.new(
				:start => 0, :end => 0,
				:rule => @rule_s,
				:dot => EarleyParser::Dot.before(@rule_s)
		)
		expect { @set.add(new_item) }.to change{@set.length}.from(0).to(1)
		
		@set.should include(new_item)
		@set.first.should eq new_item
	end
	
	it "should get item"do
		new_item = EarleyParser::Item.new(
				:start => 0, :end => 0,
				:rule => @rule_s,
				:dot => EarleyParser::Dot.before(@rule_s)
		)
		@set.add(new_item)
		
		@set.get(
			:start => 0, :end => 0,
			:rule => @rule_s,
			:dot => EarleyParser::Dot.before(@rule_s)
		).should == new_item
	end
	
	it "should raise when adding other things than items" do
		expect{ @set.add(nil) }.to raise_error
		expect{ @set.add(1) }.to raise_error
		expect{ @set.add(:start => 1, :end => 1, :rule => nil, :dot => nil) }.to raise_error
	end
	
	it "#inactive_items should return inactive items" do
		inactive_item = EarleyParser::Item.new(
				:start => 0,
				:end => 1,
				:rule => @rule_s,
				:dot => EarleyParser::Dot.after(@rule_s)
			)
			
		@set.add(inactive_item)
		
		@set.inactive_items.should have(1).item
		@set.inactive_items.first.should eq inactive_item
	end
	
	it "should add existing item by merging completing-sets" do
		@set.add(item_s1 = EarleyParser::Item.new(
			:start => 0, :end => 0,
			:rule => @rule_s,
			:dot => EarleyParser::Dot.before(@rule_s)
		))
		@set.add(item_s2 = EarleyParser::Item.new(
			:start => 0, :end => 0,
			:rule => @rule_s,
			:dot => EarleyParser::Dot.before(@rule_s.children.first)
		))
		@set.add(item_a1 = EarleyParser::Item.new(
			:start => 0, :end => 0,
			:rule => @rule_a,
			:dot => EarleyParser::Dot.before(@rule_a),
			:completing => [item_s1].to_set
		))
		expect{
			@set.add(item_a2 = EarleyParser::Item.new(
				:start => 0, :end => 0,
				:rule => @rule_a,
				:dot => EarleyParser::Dot.before(@rule_a),
				:completing => [item_s2].to_set
			))
		}.to_not change{@set.length}
		
		item_a1.completing.should == [item_s1,item_s2].to_set
	end
	
	it "should merge sets" do
		@set.add(EarleyParser::Item.new(
			:start => 0, :end => 0,
			:rule => @rule_s,
			:dot => EarleyParser::Dot.before(@rule_s)
		))
		
		other = EarleyParser::ItemSet.new
		other.add(EarleyParser::Item.new(
			:start => 0, :end => 3,
			:rule => @rule_s,
			:dot => EarleyParser::Dot.before(@rule_s)
		))
		
		expect{ @set.merge!(other) }.to change{@set.length}.from(1).to(2)
	end
	
	it "#merge! should return self" do
		other = EarleyParser::ItemSet.new
		other.add(EarleyParser::Item.new(
			:start => 0, :end => 3,
			:rule => @rule_s,
			:dot => EarleyParser::Dot.before(@rule_s)
		))
		
		@set.merge!(other).should eq @set
	end
	
	it "#merge should return new set" do
		other = EarleyParser::ItemSet.new
		other.add(EarleyParser::Item.new(
			:start => 0, :end => 3,
			:rule => @rule_s,
			:dot => EarleyParser::Dot.before(@rule_s)
		))
		
		@set.merge(other).should_not eq @set
	end
	
	it "should merge sets with common items by merging completing-sets of the items" do
		pending
	end
end
