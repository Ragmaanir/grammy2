
describe EarleyParser do
	
	PRIVATE_METHODS = [
		:current_set,
		:predict,
		:predict_items_for,
		:scan,
		:scan_for_item,
		:item_sets,
		:completed_items_for,
		:complete,
		:successive_items_of
	]
	
	EarleyParser.send(:public,*PRIVATE_METHODS)
	
	before do
		@simple_gr = Grammar.define do
			token b => 'b'
			rule bs => (b >> bs) | b
			start x => 'a' >> bs
		end
		
		@rule_x = @simple_gr.rules[:x]
		@rule_bs = @simple_gr.rules[:bs]
		@rule_b = @simple_gr.rules[:b]
	end
	
	it "should initialize parsing" do
		parser = EarleyParser.new(@simple_gr)
		
		parser.source = "dummy"
		parser.initialize_parsing
		
		parser.item_sets.length.should == 1
		parser.item_sets.first.should be_a Set
		parser.item_sets.first.should == parser.current_set
		parser.current_set.should have(1).item
		
		parser.current_set.first.should have_properties(
			:start => 0,
			:end => 0,
			:rule => @simple_gr.start_rule,
			:dot => EarleyParser::Dot.before(@simple_gr.start_rule)
		)
	end
	
	it "should calculate successive items" do
		parser = EarleyParser.new(@simple_gr)
		
		item = EarleyParser::Item.new(
			:start => 0,
			:end => 0,
			:rule => @simple_gr.start_rule,
			:dot => EarleyParser::Dot.after(@simple_gr.start_rule.children.first)
		)
		
		parser.successive_items_of(item).should == [
			EarleyParser::Item.new(# x => 'a' >> .bs
				:start => 0,
				:end => 0,
				:rule => @simple_gr.start_rule,
				:dot => EarleyParser::Dot.before(@simple_gr.start_rule.children.last)
			),
			EarleyParser::Item.new(# bs => (.b >> bs) | b
				:start => 0,
				:end => 0,
				:rule => @simple_gr.rules[:bs],
				:dot => EarleyParser::Dot.before(@simple_gr.rules[:bs].children.first.children.first)
			),
			EarleyParser::Item.new(# bs => (b >> bs) | .b
				:start => 0,
				:end => 0,
				:rule => @simple_gr.rules[:bs],
				:dot => EarleyParser::Dot.before(@simple_gr.rules[:bs].children.last)
			),
			EarleyParser::Item.new(# b => .'b'
				:start => 0,
				:end => 0,
				:rule => @simple_gr.rules[:b],
				:dot => EarleyParser::Dot.before(@simple_gr.rules[:b])
			)
		].to_set
	end
	
	describe "#successive_items_of" do
		
		before do
			#@expressions = Grammar.define do
			#	start s => plus
			#	rule plus => plus >> '+' >> plus | lit
			#	token lit => int | str
			#	token int => '0' | (1..9) >> ~(0..9)
			#	token str => +('a'..'z' | 'A'..'Z')
			#end
			
			@recursive = Grammar.define do
				start s => as | bs
				rule as => ('a' >> as) | 'a'
				rule bs => ('b' >> bs) | '.'
			end
			
			@optional = Grammar.define do
				start s => maybe_a | some
				rule maybe_a => ['a'] >> ['a']
				rule some => 'b'
			end
			
		end
	
		it "start of recursive grammar" do
			g = @recursive
			as = g.rules[:as]
			bs = g.rules[:bs]
			
			parser = EarleyParser.new(g)
			
			item = EarleyParser::Item.new(
				:start => 0,
				:end => 0,
				:rule => g.start_rule,
				:dot => EarleyParser::Dot.before(g.start_rule)
			)
			
			parser.successive_items_of(item).should == [
				EarleyParser::Item.new( # s => (.as) | bs
					:start => 0,
					:end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.start_rule.children.first)
				),
				EarleyParser::Item.new( # s => as | (.bs)
					:start => 0,
					:end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.start_rule.children.last)
				),
				EarleyParser::Item.new( # as => ((.'a') >> as) | 'a'
					:start => 0,
					:end => 0,
					:rule => as,
					:dot => EarleyParser::Dot.before(as.children.first.children.first)
				),
				EarleyParser::Item.new( # as => ('a' >> as) | .'a'
					:start => 0,
					:end => 0,
					:rule => as,
					:dot => EarleyParser::Dot.before(as.children.last)
				),
				EarleyParser::Item.new( # bs => ((.'b') >> bs) | '.'
					:start => 0,
					:end => 0,
					:rule => bs,
					:dot => EarleyParser::Dot.before(bs.children.first.children.first)
				),
				EarleyParser::Item.new( # bs => ('b' >> bs) | .'.'
					:start => 0,
					:end => 0,
					:rule => bs,
					:dot => EarleyParser::Dot.before(bs.children.last)
				)
			].to_set
		end
	end
	
	describe "should predict" do
	
		it "one item" do
			parser = EarleyParser.new(@simple_gr)
			
			parser.source = "dummy"
			parser.initialize_parsing
			
			parser.predict_items_for(parser.current_set.first).should == [
				EarleyParser::Item.new(
					:start => 0,
					:end => 0,
					:rule => @simple_gr.start_rule,
					:dot => EarleyParser::Dot.before(@simple_gr.rules[:x].children.first)
				)
			].to_set
		end
		
		it "alternatives" do
			g = Grammar.define do
				rule hello => 'say' >> 'hello'
				rule goodbye => 'say' >> 'goodbye'
				start s => hello | goodbye
			end
			
			parser = EarleyParser.new(g)
			
			parser.source = "dummy"
			parser.initialize_parsing
			
			# Predict these six items:
			# <s => .hello | goodbye>
			# <s => hello | .goodbye>
			# <hello => .('say' >> 'hello')>
			# <hello => .'say' >> 'hello'>
			# <goodbye => .('say' >> 'goodbye')
			# <goodbye => .'say' >> 'goodbye'
			parser.predict_items_for(parser.current_set.first).should == [
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.rules[:s].children.first)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.rules[:s].children.last)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.rules[:hello],
					:dot => EarleyParser::Dot.before(g.rules[:hello])
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.rules[:goodbye],
					:dot => EarleyParser::Dot.before(g.rules[:goodbye])
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.rules[:hello],
					:dot => EarleyParser::Dot.before(g.rules[:hello].children.first)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.rules[:goodbye],
					:dot => EarleyParser::Dot.before(g.rules[:goodbye].children.first)
				)
			].to_set
		end
		
		it "optional rules" do
			g = Grammar.define do
				token a => 'aaa'
				token b => 'bbb'
				start s => [a] >> b
			end
			
			parser = EarleyParser.new(g)
			
			parser.source = "dummy"
			parser.initialize_parsing
			
			# Predict:
			# <s => .[a] >> b>
			# <s => [.a] >> b>
			# <s => [a]. >> b>
			# <s => [a] >> .b>
			# <a => .'aaa'>
			# <b => .'bbb'>
			parser.predict_items_for(parser.current_set.first).should == [
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.rules[:s].children.first)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.after(g.rules[:s].children.first)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.rules[:s].children.last)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.rules[:s].children.first.rule)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.rules[:a],
					:dot => EarleyParser::Dot.before(g.rules[:a])
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.rules[:b],
					:dot => EarleyParser::Dot.before(g.rules[:b])
				)
			].to_set
		end
		
		it "repetition rules" do
			g = Grammar.define do
				token a => 'aaa'
				token b => 'bbb'
				start s => ~a >> b
			end
			
			parser = EarleyParser.new(g)
			
			parser.source = "dummy"
			parser.initialize_parsing
			
			# Predict
			# <s => .~a >> b>
			# <s => ~.a >> b>
			# <s => ~a. >> b>
			# <s => ~a >> .b>
			# <a => .'aaa'>
			# <b => .'bbb'>
			parser.predict_items_for(parser.current_set.first).should == [
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.rules[:s].children.first)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.after(g.rules[:s].children.first)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.rules[:s].children.last)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.rules[:s].children.first.rule)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.rules[:a],
					:dot => EarleyParser::Dot.before(g.rules[:a])
				),
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.rules[:b],
					:dot => EarleyParser::Dot.before(g.rules[:b])
				)
			].to_set
		end
		
		it "and reach fixpoint" do
			g = Grammar.define do
				token a => 'aaa'
				token b => 'bbb'
				start s => ~a >> b
			end
			
			parser = EarleyParser.new(g)
			
			parser.source = "dummy"
			parser.initialize_parsing
			
			parser.predict
			
			item_count = parser.predict.length
			new_item_count = parser.predict.length
			
			item_count.should == new_item_count
		end
	
		it "and track completing-set" do
			g = Grammar.define do
				token a => 'aaa'
				token b => 'bbb'
				start s => ~a >> b
			end
			
			parser = EarleyParser.new(g)
			parser.source = "dummy"
			parser.initialize_parsing
			parser.predict
			
			# Predicted
			# <s => .~a >> b>
			# <s => ~.a >> b>
			# <s => ~a. >> b>
			# <s => ~a >> .b>
			# <a => .'aaa'>
			# <b => .'bbb'>
			
			# Completing-sets
			
			s0 = parser.current_set.get(
				:start => 0, :end => 0,
				:rule => g.start_rule,
				:dot => EarleyParser::Dot.before(g.start_rule)
			)
			
			# <s => .~a >> b>
			s1 = parser.current_set.get(
				:start => 0, :end => 0,
				:rule => g.start_rule,
				:dot => EarleyParser::Dot.before(g.rules[:s].children.first)
			)
			
			# <s => (~a). >> b>
			s2 = parser.current_set.get(
				:start => 0, :end => 0,
				:rule => g.start_rule,
				:dot => EarleyParser::Dot.after(g.rules[:s].children.first)
			)
			
			# <s => ~a >> .b>
			before_b = parser.current_set.get(
				:start => 0, :end => 0,
				:rule => g.start_rule,
				:dot => EarleyParser::Dot.before(g.rules[:s].children.last)
			)
			
			# <s => ~(.a) >> b>
			before_a = parser.current_set.get(
				:start => 0, :end => 0,
				:rule => g.start_rule,
				:dot => EarleyParser::Dot.before(g.rules[:s].children.first.rule)
			)
			
			# <a => .'aaa'>
			a = parser.current_set.get(
				:start => 0, :end => 0,
				:rule => g.rules[:a],
				:dot => EarleyParser::Dot.before(g.rules[:a])
			)
			
			# <b => .'bbb'>
			b = parser.current_set.get(
				:start => 0, :end => 0,
				:rule => g.rules[:b],
				:dot => EarleyParser::Dot.before(g.rules[:b])
			)
			
			s0.completing.should be_empty
			s1.completing.should be_empty
			s2.completing.should be_empty
			before_b.completing.should be_empty
			before_a.completing.should be_empty
			
			a.completing.should == [before_a].to_set
			b.completing.should == [before_b].to_set
		end
		
	end
	
	describe "should scan" do
		
		it "string token" do
			g = Grammar.define do
				token a => 'aaa'
				token b => 'bbb'
				start s => ~a >> b
			end
			
			parser = EarleyParser.new(g)
			
			parser.source = "aaaaaaaaabbb"
			
			parser.initialize_parsing
			parser.predict
			
			parser.scan_for_item(
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.rules[:a],
					:dot => EarleyParser::Dot.before(g.rules[:a])
				)
			).should == [
				EarleyParser::Item.new(
					:start => 0, :end => 3,
					:rule => g.rules[:a],
					:dot => EarleyParser::Dot.after(g.rules[:a])
				)
			].to_set
		end
		
		it "regex token" do
			g = Grammar.define do
				token a => /aaa/
				token b => /bbb/
				start s => ~a >> b
			end
			
			parser = EarleyParser.new(g)
			
			parser.source = "aaaaaaaaabbb"
			
			parser.initialize_parsing
			parser.predict
			
			parser.scan_for_item(
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.rules[:a],
					:dot => EarleyParser::Dot.before(g.rules[:a])
				)
			).should == [
				EarleyParser::Item.new(
					:start => 0, :end => 3,
					:rule => g.rules[:a],
					:dot => EarleyParser::Dot.after(g.rules[:a])
				)
			].to_set
		end
		
		it "range token" do
			g = Grammar.define do
				start s => 'a' | 'b'
			end
			
			parser = EarleyParser.new(g)
			
			parser.source = "b"
			
			parser.initialize_parsing
			parser.predict
			
			parser.scan_for_item(
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.rules[:s],
					:dot => EarleyParser::Dot.before(g.rules[:s].children.last)
				)
			).should == [
				EarleyParser::Item.new( # s => 'a' | ('b'.)
					:start => 0, :end => 1,
					:rule => g.rules[:s],
					:dot => EarleyParser::Dot.after(g.rules[:s].children.last)
				),
				EarleyParser::Item.new( # s => ('a' | 'b').
					:start => 0, :end => 1,
					:rule => g.rules[:s],
					:dot => EarleyParser::Dot.after(g.rules[:s])
				)
			].to_set
		end
		
		it "for all items" do
			g = Grammar.define do
				token a => 'aaa'
				token b => 'bbb'
				start s => ~a >> b
			end
			
			parser = EarleyParser.new(g)
			
			parser.source = "aaaaaaaaabbb"
			
			parser.initialize_parsing
			parser.predict
			parser.scan
			
			parser.current_set.should == [
				EarleyParser::Item.new(
					:start => 0, :end => 3,
					:rule => g.rules[:a],
					:dot => EarleyParser::Dot.after(g.rules[:a])
				)
			].to_set
		end
		
	end
	
	describe "should complete" do
		it "a set of items" do
			g = Grammar.define do
				token a => 'aaa'
				token b => 'bbb'
				start s => a >> b
			end
			
			parser = EarleyParser.new(g)
			parser.source = "aaaaaaaaabbb"
			
			parser.initialize_parsing
			parser.predict
			parser.scan
			
			pending
		end
		
		it "after scanning and making a rule inactive" do
			g = Grammar.define do
				token a => 'aaa'
				token b => 'bbb'
				start s => a >> b
			end
			
			parser = EarleyParser.new(g)
			parser.source = "aaabbb"
			
			parser.initialize_parsing
			parser.predict
			parser.scan
			
			parser.current_set.should == [
				EarleyParser::Item.new(
					:start => 0, :end => 3,
					:rule => g.rules[:a],
					:dot => EarleyParser::Dot.after(g.rules[:a])
				)
			].to_set
			
			parser.current_set.first.should be_completing(
				EarleyParser::Item.new(
					:start => 0, :end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.start_rule.children.first)
				)
			)
			
			parser.complete
			
			parser.current_set.should == [
				EarleyParser::Item.new(
					:start => 0, :end => 3,
					:rule => g.rules[:a],
					:dot => EarleyParser::Dot.after(g.rules[:a])
				),
				EarleyParser::Item.new(
					:start => 0, :end => 3,
					:rule => g.rules[:s],
					:dot => EarleyParser::Dot.after(g.rules[:s].children.first)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 3,
					:rule => g.rules[:s],
					:dot => EarleyParser::Dot.before(g.rules[:s].children.last)
				),
				EarleyParser::Item.new(
					:start => 3, :end => 3,
					:rule => g.rules[:b],
					:dot => EarleyParser::Dot.before(g.rules[:b])
				)
			].to_set
		end
		
		it "after scanning string" do
			g = Grammar.define do
				start s => 'a' >> 'b'
			end
			
			parser = EarleyParser.new(g)
			parser.source = "ab"
			
			parser.initialize_parsing
			parser.predict
			parser.scan
			
			parser.complete
			
			parser.current_set.should == [
				EarleyParser::Item.new(
					:start => 0, :end => 1,
					:rule => g.rules[:s],
					:dot => EarleyParser::Dot.after(g.rules[:s].children.first)
				),
				EarleyParser::Item.new(
					:start => 0, :end => 1,
					:rule => g.rules[:s],
					:dot => EarleyParser::Dot.before(g.rules[:s].children.last)
				)
			].to_set
			
			#parser.predict
			
			#parser.current_set.should == [
			#	EarleyParser::Item.new(
			#		:start => 0, :end => 1,
			#		:rule => complex_gr.rules[:s],
			#		:dot => EarleyParser::Dot.before(complex_gr.rules[:s].children.last)
			#	)
			#].to_set
		end
		
		it "after complex scanning" do
			g = Grammar.define do
				start s => '(' >> args? >> ')'
				rule args => arg >> ~(',' >> arg)
				rule arg => 1..9
			end
			
			parser = EarleyParser.new(g)
			
			parser.source = "(1,5,9)"
			parser.initialize_parsing
			
			parser.progress_parsing
			
			parser.item_sets[0].should == [
				EarleyParser::Item.new(
					:start => 0,
					:end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.start_rule)
				),
				EarleyParser::Item.new(
					:start => 0,
					:end => 0,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.before(g.start_rule.children[0])
				)
			].to_set
			
			#parser.item_sets[1].should == [
			#	EarleyParser::Item.new(
			#		:start => 0,
			#		:end => 1,
			#		:rule => g.start_rule,
			#		:dot => EarleyParser::Dot.after(g.start_rule.children[0])
			#	),
			#	EarleyParser::Item.new(
			#		:start => 0,
			#		:end => 1,
			#		:rule => g.start_rule,
			#		:dot => EarleyParser::Dot.before(g.start_rule.children[0])
			#	)
			#]
		end
		
		it "should complete recursively (newly completed items should complete other items)" do
			g = Grammar.define do
				start s => exp
				rule exp => number
				rule number => 0..9
			end
			
			parser = EarleyParser.new(g)
			parser.source = "1"
			parser.initialize_parsing
			
			
			parser.predict
			parser.scan
			
			parser.current_set.should == [
				EarleyParser::Item.new(
					:start => 0,
					:end => 1,
					:rule => g.rules[:number],
					:dot => EarleyParser::Dot.after(g.rules[:number])
				)
			].to_set
			
			parser.complete
			
			parser.current_set.should == [
				EarleyParser::Item.new(
					:start => 0,
					:end => 1,
					:rule => g.rules[:number],
					:dot => EarleyParser::Dot.after(g.rules[:number])
				),
				EarleyParser::Item.new(
					:start => 0,
					:end => 1,
					:rule => g.rules[:exp],
					:dot => EarleyParser::Dot.after(g.rules[:exp])
				),
				EarleyParser::Item.new(
					:start => 0,
					:end => 1,
					:rule => g.start_rule,
					:dot => EarleyParser::Dot.after(g.start_rule)
				)
			].to_set
			
			parser.should be_finished
		end
		
		it "and finish parsing"
		it "and reach fixpoint"
		
		it "the source item with its inactive predicted items"
	end
	
	describe "should parse" do
	
		it "a sequence of strings" do
			g = Grammar.define do
				start s => 'a' >> 'b' >> 'c'
			end
			
			parser = EarleyParser.new(g)
			
			parser.parse("abc")
			parser.should be_finished
		end
		
		it "repetition of strings" do
			g = Grammar.define do
				start s => ~'a'
			end
			
			parser = EarleyParser.new(g)
			
			parser.parse("")
			parser.should be_finished
			
			parser.parse("aaaaaaaaaaaaaaaaaaaaaa")
			parser.should be_finished
			
			parser.parse("a")
			parser.should be_finished
		end
		
		it "repetition of complex rules" do
			g = Grammar.define do
				start s => ~(('a' | 'b') >> 'b')
			end
			
			parser = EarleyParser.new(g)
			
			parser.parse("")
			parser.should be_finished
			
			parser.parse("bb")
			parser.should be_finished
			
			parser.parse("abbbab")
			parser.should be_finished
		end
		
		it "with #parse" do
			g = Grammar.define do
				token a => 'aaa'
				token b => 'bbb'
				start s => ~a >> b
			end
			
			parser = EarleyParser.new(g)
			
			parser.parse("aaaaaaaaabbb")
		end
		
		it "in steps with #progress_parsing" do
			g = Grammar.define do
				token a => 'aaa'
				token b => 'bbb'
				start s => ~a >> b
			end
			
			parser = EarleyParser.new(g)
			
			parser.source = "aaaaaaaaabbb"
			parser.initialize_parsing
			
			4.times do
				parser.progress_parsing
			end
			
			parser.should be_finished
		end
	end
	
	describe "should parse complex examples" do
		it "example 1" do
			g = Grammar.define do
				start s => ~('a') >> ('b' | 'c')
			end
			
			parser = EarleyParser.new(g)
			
			parser.parse("b")
			parser.should be_finished
			
			parser.parse("aaac")
			parser.should be_finished
			
			parser.parse("aaaaaaab")
			parser.should be_finished
		end
		
		it "example 2" do
			g = Grammar.define do
				start s => '(' >> args? >> ')'
				rule args => arg >> ~(',' >> arg)
				rule arg => 1..9
			end
			
			parser = EarleyParser.new(g)
			
			parser.parse("(1)")
			parser.should be_finished
			
			parser.parse("(1,5)")
			parser.parse("(1,5,9)")
		end
		
	end
end
