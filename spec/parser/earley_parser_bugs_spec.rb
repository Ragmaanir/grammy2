
describe EarleyParser do
	
	it "BUG - after a complete-step both complete and predict have to be run until no further items derived" do
		
		# Occurs: After a complete-step
		# Cause:
		# 	After the first scan of '1' the following item is present:
		# 	
		#		<0,1,arg => 1#, [args => (#arg) >> ~',']>
		#		
		#  Nothing can be done before this item is used to complete,
		#  so the complete-step is executed an produces:
		# 
		#		<0,1,args => (arg#) >> ~',', [s => #args]>
		#
		#  Now prediction adds these items (and others):
		#
		#		<0,1,args => arg >> #~',', [s => #args]>
		#		<0,1,args => arg >> ~(#','), [s => #args]>
		#		<0,1,args => (arg >> ~',')#, [s => #args]> # this item is inactive
		# 
		# The last item would complete the referenced item, but since the 
		# completion-step is already ended, it won't and the parsing will fail
		# because the completion-step only is executed on the current item set 
		# and the following scan-step will create a new item-set.
		
		g = Grammar.define do
			start s => args
			rule args => arg >> ~','
			rule arg => 1
		end
		
		parser = EarleyParser.new(g)
		
		parser.parse("1")
		parser.should be_finished
	end
	
	it "BUG - left recursion causes stack overflow" do
		
		# Occurs: In the predict-step
		# Cause:
		# 	Predict is implemented as recursive function, therefore a
		#	left-recursive rule causes infinite recursion.
		#	
		
		g = Grammar.define do
			start s => (s >> '1') | '0'
		end
		
		parser = EarleyParser.new(g)
		
		expect{
			parser.parse!("1")
		}.to_not raise_error
	end
	
	it "BUG - scan from the end of an item, not from the current position of the stream" do
		# Occurs: In scan-step
		# Error: No progress, scanned multiple characters in one scan step
		# Cause: 
		#	Was scanning from @current_position instead of 
		#	from item.end which lead to scanning of multiple 
		#	tokens in one scan-step
		#
		
		g = Grammar.define do
			start exp => ('(' >> exps? >> ')') | lit
			rule exps => exp >> ~(',' >> exp)
			token lit => +'1'
		end
		
		parser = EarleyParser.new(g)
		
		parser.parse("(1,1)")
	end
	
	it "BUG - succeed on parsing empty string" do
		g = Grammar.define do
			start s => ~'a'
		end
		
		parser = EarleyParser.new(g)
		
		parser.parse("a")
		parser.parse("")
	end
	
	it "BUG - sequence of optional rules" do
		g = Grammar.define do
			start s => s2
			rule s2 => a >> a >> a >> a
			rule a => ['a']
		end
		
		parser = EarleyParser.new(g)
		
		parser.parse("a")
	end
	
end
