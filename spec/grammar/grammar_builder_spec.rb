
describe Grammy::GrammarBuilder do

	include Grammy
	
	before do
		@grammar = Grammar.new
		@builder = GrammarBuilder.new(@grammar)
	end

	it "should build an empty grammar" do
		__r = nil
		
		@builder.build do
			__r = somerule
		end
		
		__r.should be_a Grammar::NonTerminal
	end
	
	it "should track if in grammar definition" do
		@builder.build do
			in_grammar_definition?.should == true
		end
		
		@builder.should_not be_in_grammar_definition
	end
	
	it "should build a stringrule by invoking Grammar#create_rule" do
		
		@grammar.should_receive(:create_rule).once.with(:some_rule,'a',{})
		
		@builder.build do
			rule some_rule => 'a'
		end
	end
	
	it "should build token by invoking Grammar#create_token" do
		@grammar.should_receive(:create_token).once.with(:t,/regex/,{})
		
		@builder.build do
			token t => /regex/
		end
	end
	
	it "should build skipper by invoking Grammar#create_skipper" do
		@grammar.should_receive(:create_skipper).once.with(:ws,/\s+/,{})
		
		@builder.build do
			skipper ws => /\s+/
		end
	end

end

