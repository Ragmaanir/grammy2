
describe Grammy::Grammar do

	before do
		@grammar = Grammar.new
	end

	it "#create_rule should add new rule" do
		# TODO PROBLEM: Rules have no working ==
		#expect {
		#	@grammar.create_rule(:myrule,'a',{})
		#}.to change{
		#	@grammar.rules.dup # rspec keeps object reference
		#}.from({}).to({:myrule => Grammar::StringRule.new('a')})
		
		expect {
			@grammar.create_rule(:myrule,'a',{})
		}.to change{
			@grammar.rules.length # rspec problem: cant compare rules-hash, must use length
		}.from(0).to(1)
	end
	
	
	it "should set default skipper"
	it "should contain added skipper in Grammar#skippers"
	
	it "#create_token should add token"
	it "#create_skipper should add skipper"
	it "#create_helper should add helper rule"
	it "#create_fragment should add fragment rule"
	
	it "#recursive? should return true" do
		@grammar.create_rule(:rec_rule,
			Grammar::Sequence.new([
				Grammar::StringRule.new('a'),
				Grammar::NonTerminal.new(:rec_rule)
			])
		)
		
		pending
		@grammar.recursive?.should eq true
	end
	
	it "#left_recursive? should return true" do
		@grammar.create_rule(:rec_rule,
			Grammar::Sequence.new([
				Grammar::StringRule.new('a'),
				Grammar::NonTerminal.new(:rec_rule)
			])
		)
		
		pending
		@grammar.left_recursive?.should eq true
	end
	
end
