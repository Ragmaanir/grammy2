
describe Grammy::Rules::Sequence do
	
	describe "should" do

		it "define sequence via >>" do
			g = Grammar.define do
				rule seq => 'test' >> 'other'
			end

			seq_r = g.rules[:seq]
			seq_r.should be_a Grammar::Sequence
			seq_r.should have(2).children
			seq_r.children.first.should be_a Grammar::StringRule
			seq_r.children.first.string.should == 'test'
			seq_r.children.last.should be_a Grammar::StringRule
			seq_r.children.last.string.should == 'other'
		end

		it "join long sequences" do
			g = Grammar.define do
				helper a => 'a'
				helper b => 'b'
				rule phrase => a >> b >> a
			end

			phrase = g.rules[:phrase]
			phrase.should be_a Grammar::Sequence

			phrase.should have(3).children
			
			phrase.children.each{|child| child.should be_a Grammar::NonTerminal}

			phrase.children[0].name.should == :a
			phrase.children[1].name.should == :b
			phrase.children[2].name.should == :a
		end

		it "nest sequences" do
			g = Grammar.define do
				helper a => 'a'
				helper seq => a >> a
				rule phrase => seq >> seq >> seq
			end

			phrase = g.rules[:phrase]
			phrase.should be_a Grammar::Sequence
			phrase.should have(3).children

			phrase.children.each {|child|
				child.should be_a Grammar::NonTerminal
				child.name.should == :seq
			}

			seq = g.rules[:seq]
			seq.should be_a Grammar::Sequence
		end

		it "be using skipper" do
			g = Grammar.define do
				default_skipper whitespace => ' ' | "\n" | "\t"

				token a => 'ab'
				start s => a >> a >> a
			end

			g.default_skipper.should be_a Grammar::Alternatives
			g.default_skipper.type.should == :skipper
			
			g.default_skipper.should_not be_generating_ast

			g.rules[:a].skipper.should == nil
			g.rules[:a].should_not be_using_skipper
			g.rules[:s].skipper.should == g.default_skipper
			g.rules[:s].should be_using_skipper
		end

		it "not be backtracking when defined via &-operator" do
			g = Grammar.define do
				helper a => 'ab'
				rule seq => a >> a & a >> a
			end

			seq_r = g.rules[:seq]
			seq_r.should be_a Grammar::Sequence

			seq_r.should have(4).children
			seq_r.should be_backtracking
			seq_r.children[2].should_not be_backtracking
		end

		it "not be generating subnodes for helper rules" do
			g = Grammar.define do
				helper a => 'a'
				rule b => 'b'
				rule s => a >> b
			end

			start = g.rules[:s]
			
			start.should be_generating_subnodes
			start.children[0].referenced_rule.should_not be_generating_node
			start.children[1].referenced_rule.should be_generating_subnodes
		end

	end

end
