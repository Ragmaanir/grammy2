
describe Grammy::Rules::Repetition do

	describe "should define grammar" do
		it "with repetition via range" do
			g = Grammar.define do
				rule a => 'a'
				start as => a*(3..77)
			end

			as = g.rules[:as]
			as.should be_a Grammar::Repetition
			as.repetitions.should == (3..77)
			as.should have(1).children
			as.children.first.name.should == :a
		end

		it "with constant repetition" do
			g = Grammar.define do
				rule a => 'a'
				start const => a*3
			end

			const = g.rules[:const]
			const.should be_a Grammar::Repetition
			const.repetitions.should == (3..3)
			const.should have(1).children
			const.children.first.name.should == :a
		end

		it "with one or more repetitions for unary +" do
			g = Grammar.define do
				start plus => +'a'
			end

			plus = g.rules[:plus]
			plus.should be_a Grammar::Repetition
			plus.repetitions.should == (1..Grammar::MAX_REPETITIONS)
			plus.should have(1).children
			plus.children.first.should be_a Grammar::StringRule
		end

		it "with zero or more repetitions for unary ~" do
			g = Grammar.define do
				start any => ~'a'
			end

			any = g.rules[:any]
			any.should be_a Grammar::Repetition
			any.repetitions.should == (0..Grammar::MAX_REPETITIONS)
			any.should have(1).children
			any.children.first.should be_a Grammar::StringRule
		end
	end

end
