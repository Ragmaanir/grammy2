
describe Grammy::Rules::RangeRule do
	
	describe "should define" do
		it "range from a to z" do
			g = Grammar.define do
				start lower => 'a'..'z'
			end

			g.rules[:lower].should have_properties(
				:range => 'a'..'z',
				:class => Grammar::RangeRule
			)
		end

		it "range from 1 to 9" do
			g = Grammar.define do
				start digit => '1'..'9'
			end

			g.rules[:digit].should have_properties(
				:range => '1'..'9',
				:class => Grammar::RangeRule
			)
		end
		
		it "range from 1..9 with digits" do
			g = Grammar.define do
				start digit => 1..9
			end

			g.rules[:digit].should have_properties(
				:range => '1'..'9',
				:class => Grammar::RangeRule
			)
		end
	end

end
