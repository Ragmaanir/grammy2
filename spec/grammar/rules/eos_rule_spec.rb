
describe Grammy::Rules::EOSRule do
	
	it "should define grammar with eos rule" do
		g = Grammar.define do
			start phrase => 'first' >> 'last' >> eos
		end

		g.rules[:phrase].should have(3).children
		g.rules[:phrase].children[2].should be_a Grammar::EOSRule
	end

end
