
describe Grammy::Rules::RegexRule do

	describe "should define grammar" do
		it "with regex rule" do
			g = Grammar.define do
				rule lower => /[a-z]/
			end

			g.rules[:lower].should be_a Grammar::RegexRule
			g.rules[:lower].children.should be_empty
		end
	end

end
