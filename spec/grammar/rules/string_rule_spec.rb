
describe Grammy::Rules::StringRule do

	before do
		@rule = Rules::StringRule.new("somestring")
	end
	
	it "#initialize should raise when string empty" do
		expect{ Rules::StringRule.new("") }.to raise_error("empty string not allowed")
	end
	
	it "#initialize should raise when passing children" do
		expect{ Rules::StringRule.new("str", :children => [@rule]) }.
		to raise_error(ArgumentError)#raise_error("unsupported keys: {:children=>[StringRule('somestring')]}")
	end
	
	it "#to_s should return quoted string" do
		@rule.to_s.should == "'somestring'"
	end
	
	it "#to_bnf should return the same as to_s" do
		@rule.to_bnf.should == @rule.to_s
	end
	
	it "#nullable? should always return false" do
		@rule.should_not be_nullable
	end
	
	it "#children should always be empty" do
		@rule.should have(0).children
	end
	
	it "#tokens should only contain self" do
		@rule.tokens.should == [@rule].to_set
	end

end
