
describe Grammy::Rules::NonTerminal do
	
	describe "should define" do
	
		it "non-terminal outside of grammar" do
			nt = Grammar::NonTerminal.new(:therulename)
			nt.referenced_rule_name.should == :therulename
		end
		
		it "non-optional rule" do
			g = Grammar.define do
				rule a => 'a'
				start char => a
			end

			g.rules[:char].should have_properties(
				:name => :char,
				:class => Grammar::NonTerminal,
				:referenced_rule => g.rules[:a]
			)
		end

		it "optional rule" do
			g = Grammar.define do
				rule a => 'a'
				start char => a?
			end

			g.rules[:char].should have_properties(
				:name => :char,
				:class => Grammar::OptionalRule
			)
		end
	end

end
