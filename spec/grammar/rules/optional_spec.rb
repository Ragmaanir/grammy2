
describe Grammy::Rules::OptionalRule do
	
	describe "should define" do
	
		it "optional rule via ?" do
			g = Grammar.define do
				rule a => 'a'
				start char => a?
			end

			#g.rules[:char].should have_properties(
			#	:name => :char,
			#	:class => Grammar::OptionalRule
			#)
			
			char = g.rules[:char]
			char.should be_a(Grammar::OptionalRule)
			char.rule.should be_a(Grammar::NonTerminal)
			char.rule.referenced_rule.should == g.rules[:a]
		end
		
		it "optional rule via [...]" do
			g = Grammar.define do
				start char => ['a']
			end

			g.rules[:char].should have_properties(
				:name => :char,
				:class => Grammar::OptionalRule
			)
			g.rules[:char].rule.should be_a(Grammar::StringRule)
		end
		
		it "optional rule sequence via [...]" do
			g = Grammar.define do
				start char => ['a' >> 'b']
			end

			g.rules[:char].should have_properties(
				:name => :char,
				:class => Grammar::OptionalRule
			)
			g.rules[:char].rule.should be_a(Grammar::Sequence)
			g.rules[:char].rule.should have(2).children
		end
		
		it "nested optional rule" do
			g = Grammar.define do
				start char => [['a' >> 'b']]
			end

			g.rules[:char].should be_a(Grammar::OptionalRule)
			g.rules[:char].rule.should be_a(Grammar::OptionalRule)
			g.rules[:char].rule.rule.should be_a(Grammar::Sequence)
			g.rules[:char].rule.rule.should have(2).children
		end
		
		it "optional rule and raise when array has more than one entry" do
			expect{
				g = Grammar.define do
					start char => ['a','b']
				end
			}.to raise_error
		end
		
	end
	
end
