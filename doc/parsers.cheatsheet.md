
Earley Parser
=============

Item:
- Format: `[`Dotted Rule`,` Set number containing S -> .&alpha;`]`
- Example: `[`S->a.bX,0`]`

Start Items: All dotted Items of the form X -> .&alpha;

Grammar 1
---------
		S -> Sa
		S -> a

*R*ules to derive items:
- *S*canner: `X -> ? .a ?` in Si => add `X -> ? a. ?` to Si+1
- *C*ompleter: `X -> a.` in Si and `Y -> ? .X ?` in Si => add `Y -> ? X. ?` to Si
- *P*redictor: `X -> ? .Y ?` in Si and `Y -> ?` in Grammar => add `Y => .?` to Si

*S*: Number of Item-Set the Rule started in(containting `X -> .?`)

|    S0  |S|R|  S1   |S|R|  S2   |R|  S3  |
|:-------|-|-|:------|-|-|:------|-|:-----|
|S->.Sa  |0|P|S->a.  |0|S|       | |      |
|S->.a   |0|P|S->S.a |0|C|       | |      |
|        | | |       | | |       | |      |
|--------|-|-|-------|-|-|-------|-|------|
|        | | | a     | | | a     | | a    |
|--------|-|-|-------|-|-|-------|-|------|
