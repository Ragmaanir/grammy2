
Parsing
=======

Examples
--------

* Class-Keyword used inside method
		class Outer {
			method innercls {
				c = class Inner {
				
				}
			}
		}
* ID instead of class-Keyword inside method
		class Outer {
			method m = self.class
		}
* 
	- `1.34`
	- `1.cool`
	- `1..3`
	- `(1.cool)..3`

Lexing
======

Speed-up parsing by classifying substrings of the input as tokens and even doing 
conversions like string to integer.

Auto Generate Lexer
-------------------

		token id 	=> /[a-zA-Z_][a-zA-Z0-9_]*/
		token int	=> /[1-9][0-9]*/
		
		rule exp	 => id | int | parexp
		rule parexp => '(' > (exp | range) > ')'
		rule range	 => exp > '..' > exp
		

		tokens(exp) = { id, int } U tokens(parexp) = { id, int, '(', '..', ')' }
		#first(exp) = { id, int, '(' }
		#follow(exp) = { ')', '..' }
		

Problems
--------

**Problem:**
Sometimes newlines are ignored, sometime they terminate expressions. 
Sometimes whitespaces are required, sometimes optional, sometimes 
they are not allowed. Examples:

	* `meth(a + 1)` vs `meth (a + 1)`: whitespace distinguishes interpretation
	* `a + \n 1` (ok) vs `a \n + 1` (error/two expressions)
	* `[a,\n 1]` (ok) vs `[a\n, 1]` (error?)
		

**Idea:**
Lexer generates newline tokens. When parser does not expect them, 
it skips the newline token without error. Otherwise it consumes them.

**Problem:**
Sometimes 'class' is a keyword, sometime it is an id-token.

**Idea:**
A. Stateful parser
B. Ambiguous tokens: When finding the string 'class' create a class/id-token. The parser choses one of them.

**Idead:**
Primary & Secondary Skippers. The default skipper is the primary skipper 
and always used when sequences of rules are constructed via '>>'. But 
when '>' is used, then the secondary skipper is used. Also, the skipper 
can be chosen by name:

	* `a >> b` use primary skipper
	* `a > b` use secondary skipper
	* `a > skipper(:tertiary_skipper) > b` other skipper is used between `a` and `b`
	

