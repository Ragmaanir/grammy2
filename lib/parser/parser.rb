
class Grammy::Parser

	# Stores the result of a call to Parser#parse.
	# start_pos, end_pos: start and end position of the matched string in the stream # TODO start_pos always == 0?
	# tree: the generated AST
	# errors: list of errors that occurred during parsing
	# match: returns the type of match: :full, :partial, :none
	class ParseResult
		attr_reader :start_pos, :end_pos, :tree, :errors
		
		def initialize(match,context)
			raise unless match.is_a? Grammy::MatchResult
			@result = match.success?
			@start_pos, @end_pos = match.start_pos, match.end_pos
			@tree = match.ast_node
			@stream = context.stream
			@errors = context.errors
		end
		
		def range
			start_pos..end_pos
		end

		def full_match?
			match == :full
		end

		def partial_match?
			match == :partial
		end

		def no_match?
			match == :none
		end

		def has_errors?
			@errors.any?
		end

		def match
			if @result and @end_pos == @stream.length then :full
			elsif @result then :partial
			else
				:none
			end
		end

		def to_s
			<<-STR.gsub(/^\s+/,'')
			===== PARSE-RESULT =====
			Match: #{match.to_s}, range: #{@start_pos}-#{@end_pos}
			Errors(#{errors.length}):
			#{errors.map(&:to_s).join}
			=========================
			STR
		end
	end
	
	attr_reader :grammar
	
	def initialize(grammar)
		@grammar = grammar
	end
	
	def parse
		raise NotImplementedError
	end

=begin
	# stream: must behave like a string
	# options:
	# - rule: the name(symbol) of the rule to start parsing with, default is the start rule
	# - debug: true for extra debug output, default is false
	# - ast_module: module that will extend all AST-Nodes
	def parse(stream,options={})
		raise("no start rule supplied") unless @start_rule || options[:rule]
		rule = @start_rule
		rule = @rules[options[:rule]] || raise("rule '#{options[:rule]}' not found") if options[:rule]
		logger.level = DEBUG if options[:debug]

		logger.debug("##### Parsing(#{options[:rule]}): #{stream.inspect}")

		#context = Grammy::ParseContext.new(self,nil,stream,options.only(:ast_module))
		context = Grammy::ParseContext.new(self,options[:source],stream,options.only(:ast_module))

		begin
			match = rule.match(context)
		rescue Exception => e
			# TODO debug only
			puts e
			puts e.backtrace
			Log4r::NDC.clear
			logger.level = WARN if options[:debug]
			raise e
		end
		
		result = ParseResult.new(match,context)
		logger.debug("##### success: #{result.match}")

		logger.level = WARN if options[:debug]

		if options[:debug]
			puts result
			result.tree.to_image('DEBUG_'+rule.name)
		end
		result
	end

	# shortcut for:
	#		parse('...',debug: true)
	def parse!(stream,options={})
		parse(stream,options.merge(debug: true))
	end
=end
end
