
require 'set'
require 'extensions/hash'
require 'parser/parser'


#
# CURRENT IMPLEMENTATION PROBLEMS
# ===============================
# 
# Completing of items:
# Tracking of completing-sets in the predict-step is difficult:
# Completing-set is initialized when a nonterminal is reached 
# by the dot in the prediction-step. But only the item with the dot 
# right in front of that NonTerminal is tracked in the completing-set. 
# All other items with the dot in earlier positions are not.
# Is that correct????
#

class EarleyParser < Grammy::Parser

	class Lexer# < Grammy::Lexer
		
		class Token
			attr_reader :stream, :interpretations
			
			def start_at
			end
			
			def end_at
			end
			
			def length
				end_at - start_at
			end
			
			def string
				@string ||= @stream[start_at,length]
			end
			
			def line_number
			end
			
			def start_column
			end
			
			def end_column
			end
			
			def recognized_by?(rule)
				interpretations.include?(token_ids[rule])
			end
		end
		
		attr_reader :source, :current_token, :current_position
		
		# TODO token buffer
	
		# *options:*
		# 	- :text
		#	- :file
		def initialize(options={})
			if options[:text]
				@source = options[:text]
			elsif options[:file]
				@source = File.new(options[:file],'r')
			end
			
			@current_position = 0
			tokenize
		end
		
		def tokenize
			case(@source)
				when String
					grammar.tokens.each do |token|
						#@source[@current_position]
						@source[@current_position..-1] == token.string # TODO
					end
				when File
				
				else raise
			end
		end
		
		def next_token
			
		end
		
		def next_token!
			
		end
	end
	
	#
	# class Dot
	#
	class Dot
		attr_reader :rule, :position
		
		def initialize(position,rule)
			#@rule, @position = options.slice(:rule,:position)
			@rule, @position = rule, position
			raise("@rule is no rule: #{@rule.inspect}") unless @rule.is_a? Grammy::Rules::Rule
			raise("@position is invalid: #{@position.inspect}") unless [:before,:after].include? @position
		end
		
		def ==(other)
			other.is_a?(Dot) and (
				@rule.equal?(other.rule) and
				@position == other.position
			)
		end
		
		alias_method :'eql?', :'=='
		
		def hash
			@hash ||= [@rule.object_id,@position].hash
		end
		
		def inspect
			"Dot(#{@position.inspect} #{@rule.inspect})"
		end
		
		def to_s
			"#{position} #{@rule.to_s}"
			#if position == :before
			#	"#(#{@rule.to_s})"
			#else
			#	"#{(@rule.to_s)}#"
			#end
		end
		
		def self.before(rule)
			Dot.new(:before,rule)
		end
		
		def self.after(rule)
			Dot.new(:after,rule)
		end
	end
	
	#
	# class Item
	# ================================
	# An Earley-Item has the form:
	# 	Item = <start,end,dotted rule>
	# Eg: 
	# 	<0,3,X => 'aaa'. >> (B | C)>
	# 	<3,8,X => 'aaa' >> (B | C).>
	# 
	# - start: position in the stream the item starts at
	# - end: position in the stream the item ends at
	# - rule: the rule the item represents
	# - dot: indicates the position of the dot in the rule
	# - completing: set of all items that will be completed by this item when it gets inactive
	#
	# COMPLETION:
	# An item I can be completed by another item O when:
	# 	I.active?
	# 	O.inactive? (=> I.dot.position == :after)
	# 	I.end == O.start
	# 	I.dot.position == :before && I.dot.rule == O.rule
	# 	#I.rule !=O.rule # wrong: X => X>>Y
	# Eg:
	# 	I = <0,5,S => 'a' >> B. >> C >
	# 	O = <5,7,C => 'cc'.>
	# 
	# Instead of querying all items and testing them for wether they 
	# can complete another item, the set of items that can be completed 
	# by an item I is stored in the item itself. This set is generated 
	# and updated in the predict-step. An item P that is predicted from 
	# an item I can complete that item I. An item P can complete 
	# multiple other items I0..In, but all these items I0..In have 
	# already registered themselves in the completing-set during their
	# predict-step.
	#
	class Item
		attr_reader :start, :end, :rule, :dot, :completing
		
		def initialize(options)
			raise unless options.is_a? Hash
			
			@start,@end,@rule,@dot,@completing = options.slice(:start,:end,:rule,:dot,:completing)
			@completing ||= [].to_set
			
			raise(ArgumentError,"invalid @start/@end: #{@start}/#{@end}") unless @start.is_a? Integer and @end.is_a? Integer
			raise(ArgumentError,"invalid @rule") unless @rule.is_a? Grammy::Rules::Rule
			raise(ArgumentError,"invalid @dot") unless @dot.is_a? Dot
			raise unless @completing.is_a? Set
			raise("dot.rule is not a subrule of item.rule") unless @rule.has_subrule? @dot.rule
			raise unless @rule.grammar.rules.values.include? @rule
		end
		
		def inactive?
			@dot.position == :after and @dot.rule == @rule
		end
		
		alias_method :complete?, :inactive?
		
		def active?
			not inactive?
		end
		
		def completing?(*items)
			#@completing.include?(item)
			items.to_set.subset? @completing
		end
		
		def self.create_from(previous_item, options)
			options = options.with_default(
				:start => previous_item.start,
				:end => previous_item.end,
				:rule => previous_item.rule,
				:dot => previous_item.dot,
				:completing => previous_item.completing.dup
			)
			
			return Item.new(options)
		end
		
		# Two items are equal if start,end,rule and dot are equal.
		# The completing-set does not matter for that comparison.
		def ==(other)
			other.is_a?(Item) and (
				@start == other.start and 
				@end == other.end and
				@rule == other.rule and 
				@dot == other.dot
			)
		end
		
		alias_method :'eql?', :'=='
		
		def hash
			#@hash ||= [@start,@end,@rule.object_id,@dot].hash
			@hash ||= [@start,@end,rule_name.object_id,@dot.hash].hash
		end
		
		def rule_name
			@rule.grammar.rules.key(@rule)
		end
		
		def inspect(mode=:shallow)
			to_s(mode)
		end
		
		def to_s(mode=:shallow)
			if mode == :shallow
				"Item(#{@start},#{@end},#{rule_name} => #{@rule.to_grammy_string(:mark => @dot.rule, :position => @dot.position)})"
			elsif mode == :deeply
				"Item:#{object_id}(#{@start},#{@end},#{rule_name} => #{@rule.to_grammy_string(:mark => @dot.rule, :position => @dot.position)},[#{@completing.map{|i| i.object_id }.join(',')}])"
			end
		end
	end
	
	#
	# class ItemSet
	#
	class ItemSet < Set
		
		#def get(item)
		#	self.find{|i| i==item}
		#end
		
		# returns an item matching:
		# a) param if param is an item
		# b) each property value if param is a hash
		def get(item_or_props)
			if item_or_props.is_a? Item
				self.find{|i| i==item_or_props}
			else
				find { |item|
					item_or_props.all? { |prop,val|
						item.send(prop) == val
					}
				}
			end
		end
		
		def active_items
			select{|item| item.active? }
		end
		
		def inactive_items
			select{|item| item.inactive? }
		end
		
		def merge!(other)
			other.each { |item| add(item) }
			self
		end
		
		def merge(other)
			raise "parameter was not an enumerable" unless other.is_a? Enumerable
			dup.merge!(other)
		end
		
		# adds an item to the set. if that item already 
		# is contained in the set, then the new item is only 
		# added to the completing-set of the existing item 
		# and is not added to the set.
		def add(item)
			raise unless item.is_a? Item
			
			if existing_item = get(item)
				existing_item.completing.merge(item.completing)
			else
				super(item)
			end
		end
		
		alias_method :<<, :add
		
		def to_s(type=:none)
			if any?
				if type == :debug
					"ItemSet[\n\t#{map{|it| it.to_s(:deeply)}.join(",\n\t")}\n]"
				else
					"ItemSet[\n\t#{map{|it| it.to_s}.join(",\n\t")}\n]"
				end
			else
				"ItemSet[]"
			end
		end
	end#ItemSet
	
	attr_reader :lexer, :parsing_steps
	attr_writer :debug
	
	def initialize(grammar)
		super(grammar)
		construct_lexer
	end
	
	def source=(source)
		raise unless source.is_a? String or source.is_a? File
		
		if source.is_a? String
			@source = source
			@stream = source
		elsif source.is_a? File
			@source = source
			#@stream = source...
			raise NotImplementedError
		end
	end
	
	def parse(token_stream,options={})
		self.source = token_stream
		initialize_parsing
		old_item_count = 0
		new_item_count = @item_sets.map(&:length).inject(&:+)
		
		while(not finished?)
			progress_parsing
			new_item_count = @item_sets.map(&:length).inject(&:+)
			if(new_item_count == old_item_count)
				puts "--BEGIN ERROR INFO--"
				puts "input: #{@stream}"
				puts "sets: #{@item_sets.length}"
				#puts @item_sets.map{|set|
				#	set.map{|item| item.to_s(:deeply)}.join("\n")
				#}.join("\n\n")
				@item_sets.each do |set|
					if(set.any?)
						puts set.map{|item| item.to_s(:deeply)}.join("\n")
					else
						puts "[]"
					end
					puts
				end
				puts "--END ERROR INFO--"
				raise("no progress made")
			end
			old_item_count = new_item_count
		end
	end
	
	def parse!(token_stream,options={})
		puts ("-"*20)+" BEGIN PARSING "+("-"*20)
		puts "Input: '#{token_stream}'"
		self.debug = true
		parse(token_stream,options)
		self.debug = false
		puts "steps: #{@parsing_steps}"
		puts ("-"*20)+" END PARSING "+("-"*20)
	end
	
	def initialize_parsing
		raise("stream is nil") unless @stream
		@item_sets = [ItemSet.new]#[Set.new]
		
		current_set << Item.new(
			:start => 0,
			:end => 0,
			:rule => grammar.start_rule,
			:dot => Dot.before(grammar.start_rule)
		)
		
		@current_position = 0
		@parsing_steps = 0
	end
	
	def progress_parsing
		raise("stream is nil") unless @stream
		raise("parsing already finished") if finished?
		
		@parsing_steps += 1
		
		predict
		scan
		complete
	end
	
	def finished?
		#@item_sets.first.get(:rule => grammar.start_rule, :dot => Dot.after(grammar.start_rule))
		#@item_sets.first.get(:rule => grammar.start_rule, :end => @stream.length)
		#@item_sets.last.get(:rule => grammar.start_rule, :start => 0, :end => @stream.length)
		#it = @item_sets.last.get(:rule => grammar.start_rule, :start => 0, :end => @stream.length)
		@item_sets.last.inactive_items.find{ |i|
			i.rule == grammar.start_rule and
			i.start == 0 and
			i.end == @stream.length and
			i.complete?
		} != nil
	end
	
	def debugging?
		@debug
	end
	
	private
	
	attr_reader :item_sets
	
	def current_set
		raise unless @item_sets
		@item_sets.last
	end
	
	# PREDICT
	# Key:
	# 	* u,v,w		: any rules, eg: A 'b' X
	# 	* A,B,... 	: NonTerminal
	#	* a,b,c		: Terminal
	#
	# BNF
	# For Item: <x,y,A => u.Y>
	# And Rule: Y => v
	# Add Item: <y,y,Y => .v>
	#
	# EBNF
	# Repetition	: <A => .*(u)>	===> <A => *(.u)>
	# Optional		: <A => .[u]>	===> <A => [.u]>, <X => [u].>
	# Sequence		: <A => .(u)>	===> <A => (.u)>
	# NonTerminal	: <A => u.Bv>	===> <B => .w>
	# Alternatives	: <A => .(u|v)> ===> <A => (.u|v)>, <A => (u|.v)>
	def predict
		puts "PREDICTING" if debugging?
		
		new_items = ItemSet.new #[].to_set
		
		current_set.active_items.each{ |item|
			predicted_items = predict_items_for(item)
			new_items.merge!(predicted_items)
			
			#puts "items predicted from: #{item}\n#{predicted_items}" if debugging?
		}
		
		current_set.merge!(new_items)
		
		if debugging?
			puts "PREDICTED"
			puts new_items.to_s(:debug)
			puts "END PREDICTION"
		end
		
		new_items
	end
	
	def predict_items_for(item)
		new_items = ItemSet.new #Set.new
		
		# An item I can only complete item J when the following conditions hold:
		# - I = <x,y, X => u.> 		// the item is complete
		# - J = <a,x, Y => v.Xw>	// the item ends where the completed item starts and contains the NonTerminal X
		# 
		# Also: 
		# An item I can only complete items from which it has been predicted. 
		# To speed up the process of finding the rules that an item I completes,
		# store references to the predicting rules in the predicted items (stored in Item.completing)
		# 
		# when the item is already present in new_items, 
		# 	then just add all the rules the item-to-add may complete to 
		# 	the already present item's set of items it may complete
		# otherwise just add the new item
		
		# only predict when rule is active and is not a terminal
		#if(item.active? and not item.dot.rule.is_a?(Grammy::Rules::Terminal))
		if(item.active? and not (item.dot.rule.is_a?(Grammy::Rules::Terminal) and item.dot.position == :before))
			if(item.dot.position == :before)
				# item: <S => u.v>
				case(item.dot.rule)
					when Grammy::Rules::NonTerminal
						#add new item for referenced rule
						new_items << Item.new(
							:start => item.end,
							:end => item.end,
							:rule => item.dot.rule.referenced_rule,
							:dot => Dot.before(item.dot.rule.referenced_rule),
							:completing => [item].to_set
						)
						
						if item.dot.rule.nullable?
							new_items << Item.create_from(item,
								:dot => Dot.after(item.dot.rule)
							)
						end
					when Grammy::Rules::OptionalRule # item: <S => u.[v]w>
						#add two items: 
						# item <S => u[.v]w>
						# item <S => u[v].w>
						new_items << Item.create_from(item, :dot => Dot.before(item.dot.rule.rule))
						new_items << Item.create_from(item, :dot => Dot.after(item.dot.rule))
					when Grammy::Rules::Sequence # item: <S => u.(w)>
						# add item: <S => u(.w)>
						new_items << Item.create_from(item, :dot => Dot.before(item.dot.rule.children.first))
					when Grammy::Rules::Alternatives
						# add items for all alternatives: <S => u(.v|w)>, <S => u(v|.w)>, ...
						item.dot.rule.children.each do |alt|
							new_items << Item.create_from(item, :dot => Dot.before(alt))
						end
					when Grammy::Rules::Repetition
						if item.dot.rule.repetitions.include?(0)
							new_items << Item.create_from(item, :dot => Dot.after(item.dot.rule))
						end
						new_items << Item.create_from(item, :dot => Dot.before(item.dot.rule.rule))
					else
						raise "Unknown rule: #{item.dot.rule.inspect}"
				end
			elsif(item.dot.position == :after)
				# item: <S => u(v.)w>
				parent = item.dot.rule.parent
				case(parent)
					when Grammy::Rules::Sequence
						# item: <S => u(vw.)x>
						#if(item.rule == parent.children.last)
						if(item.dot.rule == parent.children.last)
							new_items << Item.create_from(item, :dot => Dot.after(parent))
						elsif(parent.children.include? item.dot.rule)
							# item has the form: <S => u(v.w)x>
							# so dot is in the middle of the sequence
							idx = parent.children.index(item.dot.rule)
							new_items << Item.create_from(item, :dot => Dot.before(parent.children[idx+1]))
						else
							raise "invalid state"
						end
					when Grammy::Rules::Repetition
						# item is: <S => u*(v.)w>
						# add item: <S => u.*(v)w>
						new_items << Item.create_from(item, :dot => Dot.before(parent))
						
						# add item: <S => u*(v).w>
						new_items << Item.create_from(item, :dot => Dot.after(parent))
					when Grammy::Rules::OptionalRule, Grammy::Rules::Alternatives
						# item is: <S => u[v.]w> or <S => u(v|x.)w>
						# add item: <S => u[v].w> or <S => u(v|x).w>
						new_items << Item.create_from(item, :dot => Dot.after(parent))
					else
						raise "Unknown parent: #{parent.inspect} of item #{item.inspect}"
				end
			else # should not happen
				raise "Invalid position: #{item.dot.position.inspect}"
			end
		end
		
		additional_items = ItemSet.new #Set.new
		
		# recursively compute all follow-up items for the newly created items
		new_items.each { |it|
			additional_items.merge!(predict_items_for(it))
		}
		
		new_items = new_items.merge(additional_items)
		
		return new_items
	end
	
	# COMPLETE
	# When Item: <x,y,A => X.Y>
	# And Item: <y,z,Y => ZW.>
	# Add Item: <x,z,A => XY.>
	def complete
		puts "COMPLETING" if debugging?
		
		new_items = ItemSet.new
		queue = Queue.new # queue of inative items
		
		# put all inactive items of the current set in the queue
		current_set.inactive_items.each{ |item|
			queue.push item
		}
		
		# complete items with the inactive items in the queue
		# add completed items that are inactive to the queue
		while queue.length > 0
			item = queue.pop
			completed_items = completed_items_for(item)
			
			completed_items.inactive_items.each{ |inactive_item|
				if not current_set.include? inactive_item
					queue.push(inactive_item)
				end
			}
			
			#new_items.merge!(completed_items)
			current_set.merge!(completed_items)
			new_items.merge!(completed_items)
		end
		
		#current_set.merge!(new_items)
		
		#begin
			#new_items = ItemSet.new
			
			## get all inactive items in the current set and use them to complete active items
			#current_set.inactive_items.each { |item|
				#completed_items = completed_items_for(item)
				#new_items.merge!(completed_items)
				
				#if debugging?
					#puts "completing with: #{item}"
					#puts "the items: \n#{item.completing.inspect}"
					#puts "and get: \n#{completed_items}"
				#end
			#}
			
			## predict here?
			
			#current_set.merge!(new_items)
			#new_items = new_items.collect{|item| not current_set.include? item }
		#end while new_items.any?
		
		#new_items = ItemSet.new
		
		## get all inactive items in the current set and use them to complete active items
		#current_set.inactive_items.each { |item|
			#completed_items = completed_items_for(item)
			#new_items.merge!(completed_items)
			
			#if debugging?
				#puts "completing with: #{item}"
				#puts "the items: \n#{item.completing.inspect}"
				#puts "and get: \n#{completed_items}"
			#end
		#}
		
		## predict here?
		
		#current_set.merge!(new_items)
		
		if debugging?
			puts "COMPLETED"
			puts new_items.to_s(:debug)
			puts "END COMPLETION"
		end
		
		new_items
	end
	
	def completed_items_for(item)
		raise unless item.is_a? Item
		new_items = ItemSet.new
		
		item.completing.each { |to_complete|
			#new_items << Item.create_from(to_complete,
			#	:end => item.end,
			#	:dot => Dot.after(to_complete.dot.rule)
			#)
			new_item = Item.create_from(to_complete,
				:end => item.end,
				:dot => Dot.after(to_complete.dot.rule)
			)
			new_items << new_item
			new_items.merge!(predict_items_for(new_item))
		}
		
		new_items
	end
	
	def scan
		puts "SCANNING" if debugging?
		
		new_items = ItemSet.new #[].to_set
		
		current_set.select{ |item| 
			item.active? and item.dot.rule.is_a? Grammy::Rules::Terminal and item.dot.position == :before
		}.each{ |item|
			#new_items.merge!(scan_for_item(item))
			
			scanned_items = scan_for_item(item)
			new_items.merge!(scanned_items)
			#puts "items scanned from: #{item}\n#{scanned_items}" if debugging?
		}
		
		@item_sets << new_items
		
		if debugging?
			puts "SCANNED"
			puts new_items.to_s(:debug)
			puts "END SCANNING"
		end
		
		new_items
	end
	
	# SCAN
	# When Item: <x,y,A => X.w>
	# And token w present
	# Add Item: <x,y+1,A => Xw.>
	def scan_for_item(item)
		raise("invalid item: dot is not before a terminal: #{item.inspect}") unless(item.dot.rule.is_a? Grammy::Rules::Terminal)
		raise("invalid item: dot is not before a terminal: #{item.inspect}") unless(item.dot.position == :before)
		
		new_items = ItemSet.new
		
		#if lexer.current_token.recognized_by?(item.dot.rule)
		#	new_items << Item.create_from(item, :dot => Dot.after(item.dot.rule))
		#	lexer.next_token!
		#else
		#	# maybe/potential error
		#end
		
		dot_rule = item.dot.rule
		
		token_length = nil
		
		# XXX skipper
		# default_skipper ws_and_nl => /.../
		# skipper ws_only => /.../
		# rule a => 'a' >> 'b' >> 'c'
		# rule b(:ws_only) => 'x' >> 'y' >> NL >> 'z'
		
		case(dot_rule)
			when Grammy::Rules::StringRule
				success = dot_rule.string == @stream[item.end,item.dot.rule.string.length]
				token_length = dot_rule.string.length if success
			when Grammy::Rules::RegexRule
				match = dot_rule.regex.match(@stream[item.end..-1])
				if match
					success = true
					token_length = match[0].length
				end
			when Grammy::Rules::RangeRule
				success = dot_rule.range.include? @stream[item.end]
				token_length = 1
			when Grammy::Rules::EOSRule
				success = item.end == @stream.length
				token_length = 0
			else raise("unknown rule: #{dot_rule.inspect}")
		end
		
		if success
			new_item = Item.create_from(item, :end => item.end+token_length, :dot => Dot.after(item.dot.rule))
			new_items << new_item
			new_items.merge!(predict_items_for(new_item))
		else
			# TODO: maybe error
		end
		
		return new_items
	end
	
	def successive_items_of(source_item)
		relevant_successors = ItemSet.new # Set of successors
		queue = Queue.new # queue of intermediate items
		
		queue.enq source_item unless source_item.complete?
		
		while queue.length > 0
		
			item = queue.pop
			
			if(item.complete?)
				new_items << item
			elsif(item.dot.position == :before)
				# item: <S => u(.v)w>
				case(item.dot.rule)
					when Grammy::Rules::Terminal
						relevant_successors << item
					when Grammy::Rules::NonTerminal
						# item: <S => u(.V)w>
						# add current item because it is relevant for completion
						relevant_successors << item
						# process successor: <V => .w>
						queue.enq Item.new(
							:start => item.end,
							:end => item.end,
							:rule => item.dot.rule.referenced_rule,
							:dot => Dot.before(item.dot.rule.referenced_rule),
							:completing => [item].to_set
						)
						
						if item.dot.rule.nullable?
							# since the rule is nullable, items with the dot after the rule are successors too
							queue << Item.create_from(item,
								:dot => Dot.after(item.dot.rule)
							)
						end
					when Grammy::Rules::OptionalRule
						# item: <S => u.[v]w>
						# process successors: <S => u[.v]w>, <S => u[v].w>
						queue.enq Item.create_from(item, :dot => Dot.before(item.dot.rule.rule))
						queue.enq Item.create_from(item, :dot => Dot.after(item.dot.rule))
					when Grammy::Rules::Sequence
						# item: <S => u.(w)>
						# process successor: <S => u(.w)>
						queue.enq Item.create_from(item, :dot => Dot.before(item.dot.rule.children.first))
					when Grammy::Rules::Alternatives
						# item: <S => u.(v|w)>
						# process successors: <S => u(.v|w)>, <S => u(v|.w)>, ...
						item.dot.rule.children.each do |alt|
							queue.enq Item.create_from(item, :dot => Dot.before(alt))
						end
					when Grammy::Rules::Repetition # items: <S => u(.~v)w>, <S => u(.+v)w>
						# process successors: <S => u(~.v)w>, maybe <S => u(~v).w>
						if item.dot.rule.repetitions.include?(0)
							queue.enq Item.create_from(item, :dot => Dot.after(item.dot.rule))
						end
						queue.enq Item.create_from(item, :dot => Dot.before(item.dot.rule.rule))
					else
						raise "Unknown rule: #{item.dot.rule.inspect}"
				end
			elsif(item.dot.position == :after)
				# item: <S => u(v.)w>
				parent = item.dot.rule.parent
				case(parent)
					when Grammy::Rules::Sequence
						if(item.dot.rule == parent.children.last)
							# item: <S => u(ab.)w>
							# process successor: <S => u((ab).)w>
							queue.enq Item.create_from(item, :dot => Dot.after(parent))
						#elsif(parent.children.include? item.dot.rule)
						else
							# item: <S => u(a.bc)w>
							# process successor: <S => u(ab.c)w>
							idx = parent.children.index(item.dot.rule)
							queue.enq Item.create_from(item, :dot => Dot.before(parent.children[idx+1]))
						#else
						#	raise "invalid state"
						end
					when Grammy::Rules::Repetition
						# item: <S => u*(v.)w>
						# process successor: <S => u.*(v)w>
						queue.enq Item.create_from(item, :dot => Dot.before(parent))
						
						# process successor: <S => u*(v).w>
						queue.enq Item.create_from(item, :dot => Dot.after(parent))
					when Grammy::Rules::OptionalRule, Grammy::Rules::Alternatives
						# item: <S => u[v.]w>, <S => u(v|x.)w>
						# process successor: <S => u[v].w> or <S => u(v|x).w>
						queue.enq Item.create_from(item, :dot => Dot.after(parent))
					else
						raise "Unknown parent: #{parent.inspect} of item #{item.inspect}"
				end
			else # should not happen
				raise "Invalid position: #{item.dot.position.inspect}"
			end
			
		
		end#while
		
		return relevant_successors
	end
	
	def construct_lexer
		
	end
	
end
