
require 'grammar/rules/rules'

module Grammy

#
#
#
#
class GrammarBuilder

	def self.build(grammar,&block)
		raise unless grammar.is_a? Grammar
		
		builder = GrammarBuilder.new(grammar)
		return builder.build(&block)
	end

	def initialize(grammar)
		raise("grammar expected") unless grammar.is_a? Grammar
		@grammar = grammar
	end
	
	def build(&block)
		raise("internal error: @grammar invalid") unless @grammar.is_a? Grammar
		
		# Classes that should get extra methods added
		classes = [String,Range,Array,Regexp]

		begin
			@in_grammar_definition = true
			classes.each{|c| Operators.inject_into(c) }
			instance_exec(&block)
		ensure
			classes.each{|c| Operators.remove_from(c) }
			@in_grammar_definition = false
		end
		
		return @grammar
	end
	
	def in_grammar_definition?
		@in_grammar_definition
	end
	
	# === This module represents the GrammarBuilder DSL
	#
	module DSL
	
		include Grammy::Rules

		def rule(options)
			@grammar.create_rule(*__prepare_parameters(options))
		end

		def skipper(options)
			@grammar.create_skipper(*__prepare_parameters(options))
		end

		def default_skipper(options={})
			@grammar.default_skipper = skipper(options)
		end

		def token(options)
			@grammar.create_token(*__prepare_parameters(options))
		end
		
		def fragment(options)
			@grammar.create_fragment(*__prepare_parameters(options))
		end
		
		def helper(options)
			@grammar.create_helper(*__prepare_parameters(options))
		end

		def start(options)
			@grammar.start_rule = rule(options)
		end

		def list(rule,sep=',',options={})
			raise unless rule.is_a? Symbol or rule.is_a? Rule
			
			rule = NonTerminal.new(rule) if rule.is_a? Symbol
			
			result = if options[:range]
					rule >> (sep & rule)*options[:range]
				else
					rule >> ~(sep & rule)
				end
			
			return result
		end

		def list?(*params)
			# TODO maybe chose better name for helper rule
			name = "list_helper_#{params.first}".to_sym
			helper(name => OptionalRule.new(rule: list(*params)))
			return NonTerminal.new(name)
		end

		def eos
			EOSRule.new
		end
		
		def method_missing(meth,*args)
			if in_grammar_definition?
				raise("no arguments supported yet for method: #{meth}") if args.any?
				# TODO use args?
				case(meth)
					when /.+[?]\Z/
						OptionalRule.new(NonTerminal.new(meth[0..-2].to_sym))
					when /.+!\Z/
						raise NotImplementedError
					else
						NonTerminal.new(meth)
				end
			else
				super(meth,*args)
			end
		end
		
		private
		
		def __prepare_parameters(options)
			raise("invalid parameters passed: #{options}") unless options.is_a? Hash and options.any?
			
			name,defn = options.shift
			name = name.referenced_rule_name.to_sym if name.is_a? NonTerminal
			
			[name,defn,options]
		end

	end
	
	include DSL

end

end#Grammy
