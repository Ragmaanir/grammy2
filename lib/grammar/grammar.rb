
require 'ast/ast'
require 'grammar/grammar_builder'
require 'grammar/rules/rules'
require 'extensions/hash'

require 'rgl/implicit'
require 'rgl/topsort'

module Grammy

#
#
#
#
class Grammar

	include Grammy::Rules

	attr_accessor :name
	attr_reader :rules, :start_rule
	
	def self.define(name=nil,&block)
		grammar = Grammar.new(name)
		return GrammarBuilder.build(grammar,&block)
	end

	def initialize(name=nil)
		raise unless name==nil or [String,Symbol].include? name.class
		
		@name = name
		@rules = {}
	end
	
	def create_rule(name,definition,options={})
		raise "invalid name('#{name}') for rule" unless name.is_a? Symbol
		#raise "invalid definition for rule '#{name}'" unless definition.is_a? Rule
		raise "rule '#{name}' defined twice" if @rules.member?(name)
		raise "options must be a hash but was: #{options}" unless options.is_a? Hash
		
		# TODO move to rule or create a constant for defaults
		options = options.with_default(
			:skipper => default_skipper ? default_skipper.name : nil,
			:generating_ast => true,
			:generating_subnodes => true,
			:generating_node => true,
			:debug => :all,
			:type => :rule
		)
		
		rule = Rule.to_rule(definition)

		rule.setup(options.merge(:name => name))
		rule.grammar = self
		
		@rules[name] = rule
	end
	
	def create_skipper(name,definition,options={})
		options = options.
					with_default(debug: :root_only).
					merge(skipper: nil, generating_ast: false, type: :skipper)
		
		create_rule(name,definition,options)
	end
	
	# creates a rule with these options:
	# - does not use skipper
	# - generates only one AST-node, no subnodes
	def create_token(name,definition,options={})
		options = options.
					with_default(debug: :root_only).
					merge(skipper: nil, generating_subnodes: false, type: :token)
		
		create_rule(name,definition,options)
	end
	
	# Creates a rule that
	# - doesn't use a skipper
	# - doesn't create AST-nodes
	def create_fragment(name,definition,options={})
		options = options.
					with_default(debug: :none).
					merge(skipper: nil, generating_ast: false, type: :fragment)
		
		create_rule(name,definition,options)
	end
	
	# generates no extra AST-node
	def create_helper(name,definition,options={})
		options = options.
					merge(generating_node: false, type: :helper)
		
		create_rule(name,definition,options)
	end
	
	def default_skipper
		@default_skipper
	end
	
	def default_skipper=(s)
		raise unless s
		raise "Default skipper already set to: #{default_skipper.name}" if default_skipper
		@default_skipper = s
	end
	
	def start_rule=(r)
		raise unless r.is_a? Rule
		raise("attempting to redefine start_rule(#{@start_rule}) as #{r}") if @start_rule
		
		@start_rule = r
	end
	
	def using_skippers?
		skippers.any?
	end

	# returns the hash of all rules that are registered as a skipper
	def skippers
		#@skippers ||= @rules.select{ |_,rule| rule.type == :skipper }
		@rules.select{ |_,rule| rule.type == :skipper }
	end
	
	def tokens
		tokens = [].to_set
		
		@rules.each { |r|
			if r.type == :skipper
				tokens << r
			elsif r.type == :token
				tokens << r
			else
				r.tokens
			end
		}
	end

	def validate
		raise NotImplementedError # TODO implement
		# check for always fail		: ~a >> a
		# check for left recursion: x => x | y
		# check that tokens are not nested
		# warn if tokens/rules are unused
		# check for non-productive rules: x => 'a' >> x
		# check for loops: x => y, y => x
		# check for unit rules: x => y | z
		# check for doubled rules: [[x]], ~~x
		# check for +(x?) and warn when [+x], which is equal to *x
		# check for x >> EOF >> y
		# pairwise '(',')' #=> ensure they occur pairwise in grammar
		# warn when x => y | y
	end
	
	def recursive?
		not(RGL::ImplicitGraph.new { |g|
			g.vertex_iterator { |block|
				rules.each(&block)
			}
			g.adjacent_iterator { |vertex, block|
				name,rule = *vertex
				rule.referenced_non_terminals.each(&block)
			}
			g.directed = true
        }.acyclic?)
	end
	
	def left_recursive?
		raise NotImplementedError
	end
	
	# Returns true, false or unknown
	def ambiguous?
		raise NotImplementedError
	end
	
	def to_bnf_grammar
		raise NotImplementedError
	end
end

end#Grammy
