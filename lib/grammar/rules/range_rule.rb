
module Grammy::Rules

	#
	# RangeRule
	#
	class RangeRule < Terminal
		attr_reader :range

		def initialize(range,options={})
			super(options)
			raise "range must be range but was: '#{range}'" unless range.is_a? Range
			raise("range includes empty string") if range.include? ""
			range = range.min.to_s..range.max.to_s unless range.min.is_a? String
			@range = range
		end

		def to_s
			"(#{@range.min}..#{@range.max})"
		end

		def to_bnf
			to_s
		end
	end # RangeRule

end # Grammy::Rules
