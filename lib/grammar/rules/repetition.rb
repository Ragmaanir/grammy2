
module Grammy::Rules

	#
	# REPETITION
	#
	class Repetition < StructuralRule
		attr_accessor :repetitions
		attr_reader :rule

		def initialize(rule,times,options={})
			#@rule = Rule.to_rule(options.delete(:rule) || raise("no rule given"))
			@rule = Rule.to_rule(rule)
			@rule.parent = self
			@repetitions = times
			#@repetitions = options.delete[:times] || raise("no repetition supplied")
			super(options)
		end

		def children
			[@rule]
		end
		
		def nullable?
			repetitions.include?(0) or rule.nullable?
		end

		def to_s
			rule_str = @rule.to_s
			rule_str = "(#{rule_str})" if [Sequence,Alternatives,Repetition].include? @rule.class

			if repetitions == (0..MAX_REPETITIONS)
				"~#{rule_str}"
			elsif repetitions == (1..MAX_REPETITIONS)
				"+#{rule_str}"
			elsif repetitions.min == repetitions.max
				"#{rule_str}*#{repetitions.min}"
			else
				"#{rule_str}*#{repetitions}"
			end
		end
		
		def to_grammy_string(options={})
			str = @rule.to_grammy_string(options)
			str = "(#{str})" if [Sequence,Alternatives,Repetition].include? @rule.class
			
			str = if repetitions == (0..MAX_REPETITIONS)
					"~#{str}"
				elsif repetitions == (1..MAX_REPETITIONS)
					"+#{str}"
				elsif repetitions.min == repetitions.max
					"#{str}*#{repetitions.min}"
				else
					"#{str}*#{repetitions}"
				end
			
			if options[:mark] == self
				str = str.on_blue
				str = case options[:position]
					when :after then str + "#".red
					when :before then "#".red + str
					else raise
				end
			end
			
			str
		end

		def to_bnf
			rule_str = @rule.to_bnf
			rule_str = "(#{rule_str})" if [Sequence,Alternatives,Repetition].include? @rule.class

			if repetitions == (0..MAX_REPETITIONS)
				"#{rule_str}*"
			elsif repetitions == (1..MAX_REPETITIONS)
				"#{rule_str}+"
			elsif repetitions.min == repetitions.max
				"#{rule_str}[#{repetitions.min}]"
			else
				"#{rule_str}[#{repetitions}]"
			end
		end
	end # Repetition

end # module Grammy::Rules
