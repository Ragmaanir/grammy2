
require 'grammar/rules/operators'

module Grammy::Rules

	#
	# RULE
	#
	class Rule
		Operators.inject_into(self)

		Callbacks = [:modify_ast,:on_error,:on_match]
		#Options = [:backtracking,:skipper,:generating_node,:generating_subnodes,:generating_ast,:debug,:type,:times,:optional,:children,:name] + Callbacks
		#Options = [:skipper,:generating_node,:generating_subnodes,:generating_ast,:debug,:type,:times,:optional,:children,:name] + Callbacks
		Options = [:skipper,:generating_node,:generating_subnodes,:generating_ast,:debug,:type,:name] + Callbacks
		DebugModes = [:like_root,:all,:root_only,:none]

		attr_accessor :parent
		attr_reader :name, :grammar, :options, :debug, :type
		#attr_writer :backtracking

		def initialize(options={})
			setup(options)
		end

		def setup(options={})
			raise("hash required") unless options.is_a? Hash
			@name = options[:name]

			unsupported = options.reject{ |key,_| Options.include? key }
			raise(ArgumentError,"unsupported keys: #{unsupported.inspect}") if unsupported.any?

			options = options.with_default(
				#backtracking: true,
				skipper: nil,
				debug: :like_root,
				#type: :anonymous,
				generating_ast: true,
				generating_subnodes: true,
				generating_node: true
			)
			
			@callbacks = options.extract!(*Callbacks)

			@generating_subnodes, @generating_node, @generating_ast = options.slice(:generating_subnodes,:generating_node,:generating_ast)
			
			#@backtracking,@skipper,@debug,@type = options.slice(:backtracking,:skipper,:debug,:type)
			@skipper,@debug,@type = options.slice(:skipper,:debug,:type)

			raise "@generating_* should be booleans" if [@generating_subnodes, @generating_node, @generating_ast].any?{ |attr| not [true,false].include?(attr) }
			raise "invalid debug mode: #{@debug.inspect}" unless DebugModes.include? @debug
			raise unless [true,false].include? @generating_ast
			raise "invalid skipper: #{@skipper}" unless @skipper==nil or @skipper.is_a? Symbol
		end

		# The #root method returns the production that the rule is part of.
		# 
		# *Example*
		# for the rule:
		#		rule x: :a >> +'x'
		# the following
		#		x.children[1].children[0].root
		# returns x
		def root
			cur_rule = self
			while cur_rule.parent
				cur_rule = cur_rule.parent
			end

			cur_rule
		end

		def anonymous?
			@name==nil
		end

		# Returns true iff debugging is turned on for this rule.
		# The method takes into account the setting for the root of the rule:
		# - when the debug mode of the root rule is set to :all, then all subrules have debugging turned on
		# - when the debug mode of the root rule is set to :root_only, then only the root rule has debugging turned on
		def debugging?
			if root.debug == :all
				true
			elsif root.debug == :root_only
				root == self
			else
				false
			end
		end

		# For debugging purposes: returns the classname
		def class_name
			self.class.name.split('::').last
		end

		# #TRUE for most rules, FALSE for e.g. tokens
		# When set to true then the parser should generate AST-nodes
		# for the child-rules of this rule.
		def generating_subnodes?
			@generating_subnodes and generating_ast?
		end
		
		# # TRUE for most rules, FALSE for helper e.g. rules
		# When set to true then the parser should generate a node 
		# for this rule. 
		def generating_node?
			@generating_node and generating_ast?
		end
		
		# # TRUE for tokens, rules, helpers, FALSE for skippers
		# Used to enable/disable total AST generation for this rule and its subrules.
		def generating_ast?
			@generating_ast
		end

		# When backtracking then no errors are generated when the parser 
		# fails to parse with this rule.
		# TODO maybe deprecated, parser should decide wether an parsing-error occurred
		# DEPRECATED
		#def backtracking?
		#	@backtracking
		#end

		def using_skipper?
			grammar.using_skippers? and skipper
		end

		def skipper
			if root == self
				grammar.skippers[@skipper]
			else
				root.skipper
			end
		end

		def children
			raise NotImplementedError("implemented in subclasses")
		end

		def grammar=(gr)
			raise unless gr.is_a? Grammy::Grammar
			@grammar = gr
			children.each{|child|
				child.grammar = gr
			}
		end
		
		def has_subrule?(rule)
			(rule == self) or
			children.any? { |child|
				child.has_subrule?(rule) 
			}
		end
		
		def referenced_non_terminals
			raise NotImplementedError("implemented in subclasses")
		end
		
		# TODO rename 'referenced_tokens' ?
		def tokens
			raise NotImplementedError("implemented in subclasses")
		end

		def to_s
			#class_name + '{' + children.join(',') + '}'
			raise NotImplementedError("implemented in subclasses")
		end
		
		def to_grammy_string(options={})
			raise NotImplementedError
		end
		
		def to_bnf# TODO rename: to_ebnf
			raise NotImplementedError
		end

		def to_image(name)
			raise NotImplementedError
			require 'graphviz'
			graph = GraphViz.new(name)
			graph.node[shape: :box, fontsize: 8]

			to_image_impl(graph)
		end
		
		def inspect(format=:short)
			if(format==:short)
				"#{class_name}(#{to_s})"
			else
				super
			end
		end
		
		# returns true iff there is a derivation that produces epsilon (the empty string)
		def nullable?
			raise NotImplementedError("implemented in subclasses")
		end
		
		# PROBLEM: 
		# rule x => y? >> z
		# x.first_set #=>
		# a) [y,z]
		# b) [y,skipper] # if skipper contains no epsilon production
		# c) [y,skipper,z] # if skipper is optional/can match epsilon
		def first_set
			
		end
		
		# Converts the input to instances subclasses of the Rule class.
		# Maps:
		# 	Range => RangeRule
		# 	Array => OptionalRule
		#	Symbol => OptionalRule/NonTerminal
		#	String => StringRule
		#	Integer => StringRule
		#	Regexp => RegexRule
		#	Rule => Rule
		#	_ => error
		def self.to_rule(input)
			case input
				when Range then RangeRule.new(input)
				when Array then
					raise("array has multiple entries but should have only one") unless input.length == 1
					OptionalRule.new(Rule.to_rule(input.first))
				when Symbol then
					if /.+[?]\Z/ === input
						OptionalRule.new(NonTerminal.new(input[0..-2].to_sym))
					else
						NonTerminal.new(input)
					end
				when String,Integer then StringRule.new(input.to_s)
				when Regexp then RegexRule.new(input)
				when Rule then input
				else raise "invalid input '#{input}', cant convert to a rule"
			end
		end
	end

	#
	# Terminal
	# Terminal rules have no children. 
	# Examples: StringRule, RegexRule, EOSRule
	class Terminal < Rule
		def children
			[]
		end
		
		def referenced_non_terminals
			[]
		end
		
		def tokens
			[self].to_set
		end
		
		def nullable?
			#raise NotImplementedError("implemented in subclasses")
			false
		end
		
		def to_grammy_string(options={})
			str = to_s
			if options[:mark] == self
				str = str.on_blue
				str = case options[:position]
					when :after then str + "#".red
					when :before then "#".red + str
					else raise
				end
			end
			str
		end
	end
	
	#
	# StructuralRule
	# Structural Rules have children. 
	# Examples: Repetition, OptionalRule, Sequence, Alternatives
	class StructuralRule < Rule
		
		def tokens
			children.map{|c| 
				#if c.is_a? Terminal
				#	c
				#else
				#	c.tokens
				#end
				c.tokens
			}.flatten.to_set
		end
		
		def referenced_non_terminals
			children.inject([]) { |acc,child|
				acc + child.referenced_non_terminals
			}
		end
		
		def nullable?
			raise NotImplementedError("implemented in subclasses")
		end
	end

end # Grammy::Rules
