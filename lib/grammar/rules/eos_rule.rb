
module Grammy::Rules
	#
	# EOSRule
	# - matches the end of the stream
	# - when skipping: skips characters until end of stream reached
	class EOSRule < Terminal
		
		def nullable?
			false
		end
		
		def to_s
			"EOS"
		end

		def to_bnf
			to_s
		end
	end
end # Grammy::Rules
