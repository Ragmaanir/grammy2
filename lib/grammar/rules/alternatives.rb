
module Grammy::Rules

	#
	# ALTERNATIVE
	#
	class Alternatives < StructuralRule
		attr_reader :children

		def initialize(children,options={})
			super(options)
			@children = []
			#options.delete(:children).each { |alt|
			children.each { |alt|
				alt = Rule.to_rule(alt)
				if alt.is_a? Alternatives
					@children.concat(alt.children)
				else
					@children << alt
				end
			}

			@children.each{|c| c.parent = self }
		end
		
		def nullable?
			children.any?(&:nullable?)
		end

		def to_s
			@children.map(&:to_s).join(" | ")
		end
		
		def to_grammy_string(options={})
			str = @children.map{|item|
				item.to_grammy_string(options)
			}.join(" | ")
			if options[:mark] == self
				str = str.on_blue
				str = case options[:position]
					when :after then str + "#".red
					when :before then "#".red + str
					else raise
				end
			end
			str
		end

		def to_bnf
			"#{@children.map(&:to_bnf).join(" | ")}"
		end
	end # Alternatives

end # Grammy::Rules
