
module Grammy::Rules
	
	class OptionalRule < StructuralRule
		attr_reader :rule
		
		def initialize(rule,options={})
			raise("OptionalRule: expected rule but got: #{rule}") unless rule.is_a? Rule
			@rule = rule
			@rule.parent = self
			super(options)
		end
		
		def children
			[@rule]
		end
		
		def nullable?
			true
		end
		
		def to_bnf
			if rule.is_a? NonTerminal
				"#{rule.to_bnf}?"
			else
				"[#{rule.to_bnf}]"
			end
		end
		
		def to_grammy_string(options={})
			str = @rule.to_grammy_string(options)
			str =  if rule.is_a? NonTerminal
					"#{str}?"
				else
					"[#{str}]"
				end
			
			if options[:mark] == self
				str = str.on_blue
				str = case options[:position]
					when :after then str + "#".red
					when :before then "#".red + str
					else raise
				end
			end
			str
		end
		
		def to_s
			to_bnf
		end
	end
	
end
