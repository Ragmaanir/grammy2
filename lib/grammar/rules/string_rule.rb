
module Grammy::Rules
	#
	# StringRule
	#
	class StringRule < Terminal
		attr_reader :string

		def initialize(string,options={})
			#raise unless options[:string].is_a? String
			raise unless string.is_a? String
			raise("empty string not allowed") unless string.length > 0
			@string = string #options[:string]
			super(options)
		end

		def to_s
			"'#{@string}'"
		end

		def to_bnf
			to_s
		end
	end # StringRule

end # Grammy::Rules
