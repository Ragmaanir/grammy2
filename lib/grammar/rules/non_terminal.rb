
module Grammy::Rules

	#
	# NonTerminal
	# Wraps a symbol which represents another rule which may be not defined yet.
	class NonTerminal < Rule
		attr_reader :referenced_rule_name
		
		def initialize(rule_name,options={})
			#sym = options.delete(:rule_name)
			raise("rule_name has to be a symbol but was: '#{rule_name}'") unless rule_name.is_a? Symbol
			raise("invalid rule name: #{rule_name}") if /[?!=]/ === rule_name.to_s
			
			@referenced_rule_name = rule_name
			#super(@referenced_rule_name,options)
			super(options.merge(:name => rule_name))
		end

		def grammar=(gr)
			# dont set grammar for children because the referenced rule might not be defined yet
			# also the referenced rule has a name (defined via rule-method), so it will get assigned a grammar anyway
			@grammar = gr
		end

		def children
			[]#rule.children
		end

		def referenced_rule
			grammar.rules[@referenced_rule_name] || raise("rule not found '#{@referenced_rule_name}'")
		end
		
		def nullable?
			referenced_rule.nullable?
		end

		def to_s
			to_bnf
		end
		
		def to_grammy_string(options={})
			str = to_s
			if options[:mark] == self
				str = str.on_blue
				str = case options[:position]
					when :after then str + "#".red
					when :before then "#".red + str
					else raise
				end
			end
			str
		end

		def to_bnf
			@referenced_rule_name.to_s #name
		end
	end # RuleReference

end # Grammy::Rules
