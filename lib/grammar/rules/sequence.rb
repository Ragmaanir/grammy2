
module Grammy::Rules

	#
	# SEQUENCE
	#
	class Sequence < StructuralRule
		attr_reader :children

		def initialize(children,options={})
			super(options)
			#@children = options.delete(:children).map{ |elem|
			@children = children.map{ |elem|
				elem = Rule.to_rule(elem)
				if elem.is_a? Sequence and elem.anonymous?
					#if not elem.backtracking?
					#	elem.children.first.backtracking = false
					#end
					elem.children
				else
					elem
				end
			}.flatten

			@children.each{|c| c.parent = self }
		end
		
		def nullable?
			children.all?(&:nullable?)
		end

		def to_s
			children.map{|item|
				if item.is_a? Alternatives
					"(#{item})"
				else
					item.to_s
				end
			}.join(" >> ")
		end
		
		def to_grammy_string(options={})
			str = children.map{|item|
				str = item.to_grammy_string(options)
				str = "(#{str})" if item.is_a? Alternatives
				str
			}.join(" >> ")
			
			if options[:mark] == self
				str = str.on_blue
				str = case options[:position]
					when :after then str + "#".red
					when :before then "#".red + str
					else raise
				end
			end
			str
		end

		def to_bnf
			children.map{|item|
				bnf = item.to_bnf
				bnf = "(#{bnf})" if item.is_a? Alternatives
				bnf
			}.join(" ")
		end

		protected
		def to_image_impl(graph)
			raise NotImplementedError # TODO implement
			#
			# Problem: a >> +(b | c)
			# How to display that?
			#
			#    +-----<-----+
			#    |  +--b--+  |
			# a--+--|     |--+-->
			#       +--c--+
			#
			#
			raise "no graph supplied" unless graph
			last_node = nil
			@children.each{|item|
				new_node = graph.add_node(cur_node.data.object_id.to_s, label: "'#{cur_node.data}'")
				new_node[shape: :circle, style: :filled, fillcolor: "#6699ff", fontsize: 8]
				graph.add_edge(last_node,new_node)
			}
		end

	end # Sequence

end # Grammy::Rules
