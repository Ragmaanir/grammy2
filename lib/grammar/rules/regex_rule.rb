
module Grammy::Rules

	#
	# RegexRule
	#
	class RegexRule < Terminal
		attr_reader :regex
		
		def initialize(regex,options={})
			raise "regexp expected but got: #{regex.inspect}" unless regex.is_a? Regexp
			raise("regex accepts empty string") if regex === ""
			
			@regex = /#{regex.source}/u
			super(options)
		end

		def to_s
			"/#{regex.source}/"
		end

		def to_bnf
			to_s
		end
	end
end
